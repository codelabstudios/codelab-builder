const Generator = require('yeoman-generator')
// const run = require("gulp-run");
const { exec } = require('child_process')
const cmd = require('node-cmd')
const mkdirp = require('mkdirp')
const fs = require('fs')
const path = require('path')
const { get, set, flatten, forOwn } = require('lodash')

module.exports = class extends Generator {
  // The name `constructor` is important here
  constructor(args, opts) {
    opts.force = true
    // Calling the super constructor is important so our generator is correctly
    // set up
    super(args, opts)

    // Next, add your custom code
    this.option('babel') // This method adds support for a `--babel` flag
    this.answers = {
      packageName: '@codelab/package',
    }

    // Boolean flag to see if we want to create file
    this.packageJsonExists = false

    // Used to hold all the installations to be combined and run later
    this.yarnPackages = {
      default: [],
      dev: [],
    }
  }

  /**
   * (1) Your initialization methods (checking current project state, getting
   * configs, etc).
   */
  initializing() {
    // If package.json exists, we assume it has already been scaffolded
    this.packageJsonExists = this.fs.exists(
      this.destinationPath('package.json'),
    )
  }

  /**
   * (2) Where you prompt users for options (where you’d call this.prompt()).
   */
  async prompting() {
    this.answers = await this.prompt([
      {
        type: 'input',
        name: 'packageName',
        message: 'Your project name',
        default: this.appname, // Default to current folder name
      },
      {
        type: 'confirm',
        name: 'installPackages',
        message: 'Would you like to install NPM packages?',
      },
    ])
  }

  /**
   * (3) Saving configurations and configure the project (creating
   * .editorconfig files and other metadata files).
   */
  configuring() {
    const packageDir = this._createProjectDir()

    // Set destination path
    this.destinationRoot(packageDir)

    // Check if file exists
    if (!this.packageJsonExists) {
      this._createPackageJson()
    }
  }

  /**
   * (4) If the method name doesn’t match a priority, it will be pushed to this
   * group.
   */
  default() {}

  /**
   * (5) Where you write the generator specific files (routes, controllers,
   * etc).
   */
  writing() {
    // this.fs.copyTpl(
    //   this.templatePath("src"),
    //   this.destinationPath("src"),
    //   // this.destinationPath("pages/index.js"),
    //   { title: "Templating with Yeoman" }
    // );
  }

  /**
   * (7) Where installations are run (npm, bower).
   */
  install() {
    this._addBaseConfig()
    this._addTestConfig()
    this._yarnInstall()
  }

  /**
   * (8) Called last, cleanup, say good bye, etc.
   */
  end() {
    this._runCommand('prettier --write package.json')
    // Need to re-install yarn
    this._runCommand('rm -r node_modules && yarn')
  }

  /**
   * =================== PRIVATE METHODS ===================
   */
  _createProjectDir() {
    const packageDir = this.destinationPath(this.answers.packageName)

    if (!fs.existsSync(packageDir)) {
      fs.mkdirSync(packageDir)
    }

    return packageDir
  }

  _createPackageJson() {
    this.composeWith(require.resolve('generator-npm-init/app'), {
      // skip prompts
      'skip-name': true,
      'skip-description': true,
      'skip-version': true,
      'skip-main': true,
      'skip-test': true,
      'skip-repo': true,
      'skip-keywords': true,
      'skip-author': true,
      'skip-license': true,

      // supply alternative defaults
      name: this.answers.packageName,
      version: '0.0.1',
      description: '',
      main: 'index.js',
      test: 'echo "Error: no test specified" && exit 1',
      repo: '',
      keywords: [],
      author: '',
      license: 'ISC',

      // configure run script defaults
      scripts: {},
    })
  }

  _testUpdatePackage() {
    this._addConfigToPackageJson('scripts', {
      build: 'webpack',
    })
  }

  _queueYarnInstall(packages, options = {}) {
    const type = options && options['dev'] === true ? 'dev' : 'default'

    packages.forEach(name => {
      this.yarnPackages[type].push(name)
    })
  }

  _yarnInstall() {
    if (!this.answers.installPackages) return

    forOwn(this.yarnPackages, (packages, type) => {
      const dev = type === 'dev' ? true : false
      this.yarnInstall(packages, { dev })
    })
  }

  _addBaseConfig() {
    /**
     * Scaffold src directory
     */
    if (!this.packageJsonExists) {
      this.fs.copyTpl(this.templatePath('src'), this.destinationPath('src'))
    }
    this._queueYarnInstall(['react@16.12.0', 'react-dom@16.12.0'])

    /**
     * Watching Scripts
     */
    this._addConfigToPackageJson('scripts', {
      dev: 'ttsc -b -w --preserveWatchOutput',
    })

    /**
     * Build scripts
     */
    this._addConfigToPackageJson('scripts', {
      build: 'ttsc -b',
    })

    /**
     * Webpack
     */
    // this.fs.copyTpl(
    //   this.templatePath("webpack.config.js"),
    //   this.destinationPath("webpack.config.js"),
    //   { packageName: this.answers.packageName }
    // );
    // this._queueYarnInstall(["webpack", "webpack-cli"], { dev: true });

    /**
     * Webpack dev
     */
    // this._queueYarnInstall(["webpack-dev-server"], { dev: true });
    // this._addConfigToPackageJson("scripts", { start: "webpack-dev-server" });

    /**
     * Config scripts
     */
    this.fs.copyTpl(
      this.templatePath('.gitignore'),
      this.destinationPath('.gitignore'),
    )

    /**
     * Webpack Plugins
     */
    this._queueYarnInstall(
      ['mini-css-extract-plugin', 'clean-webpack-plugin'],
      {
        dev: true,
      },
    )

    /**
     * Typescript
     */
    this.fs.copyTpl(
      this.templatePath('tsconfig.json'),
      this.destinationPath('tsconfig.json'),
    )

    // https://github.com/Microsoft/TypeScript/issues/15479
    // ttypescript used for abs -> rel path transformation for generated types
    this._queueYarnInstall(
      [
        'typescript@3.3.1',
        'ts-loader@5.3.3',
        'ttypescript@1.5.10',
        '@zerollup/ts-transform-paths@1.7.1',
      ],
      {
        dev: true,
      },
    )
    this._addConfigToPackageJson('', {
      types: 'dist/src/index.d.ts',
    })

    /**
     * Babel
     */
    this.fs.copyTpl(
      this.templatePath('.babelrc'),
      this.destinationPath('.babelrc'),
    )
    this._queueYarnInstall(
      [
        '@babel/core@7.3.4',
        '@babel/plugin-proposal-class-properties@7.3.4',
        '@babel/plugin-transform-runtime@7.3.4',
        '@babel/preset-env@7.3.4',
        '@babel/preset-react@7.0.0',
        '@babel/preset-typescript@7.3.3',
        '@babel/runtime@7.3.4',
        'babel-jest@25.1.0',
        'babel-loader@8.0.5',
        'babel-plugin-module-resolver@3.2.0',
      ],
      {
        dev: true,
      },
    )
  }

  _addTestConfig() {
    this._addConfigToPackageJson('scripts', {
      storybook: 'start-storybook -p 9001 -c .storybook',
    })
    this._queueYarnInstall(
      [
        '@storybook/addon-actions@5.3.18',
        '@storybook/addon-info@5.3.18',
        '@storybook/addon-links@5.3.18',
        '@storybook/addons@5.3.18',
        '@storybook/react@5.3.18',
      ],
      {
        dev: true,
      },
    )

    this.fs.copyTpl(
      this.templatePath('jest.config.js'),
      this.destinationPath('jest.config.js'),
    )

    this.fs.copyTpl(
      this.templatePath('jest.setup.js'),
      this.destinationPath('jest.setup.js'),
    )
  }

  _runCommand(cmd) {
    exec(cmd, (err, stdout, stderr) => {
      if (err) {
        console.log('Error!')
        console.log(err)
        // node couldn't execute the command
        return
      }

      // the *entire* stdout and stderr (buffered)
      console.log(`${stdout}`)
      console.log(`${stderr}`)
    })
  }

  _queueAddConfigToPackageJson(path, option) {
    const type = options && options['dev'] === true ? 'dev' : 'default'

    packages.forEach(name => {
      this.yarnPackages[type].push(name)
    })
  }

  _addConfigToPackageJson(path, option) {
    const key = Object.keys(option)[0]
    const value = option[key]

    // Get config
    const packageJsonPath = this.destinationPath('package.json')
    const config = require(packageJsonPath)

    if (path) {
      config[path][key] = value
    } else {
      config[key] = value
    }

    // Write
    fs.writeFileSync(packageJsonPath, JSON.stringify(config), 'utf-8')
  }
}
