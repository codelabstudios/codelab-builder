import { action } from '@storybook/addon-actions'
import { storiesOf } from '@storybook/react'

import 'antd/dist/antd.css'
import React from 'react'
import { Context, Form } from '../src/index'

const theme = {
  color: {
    primary: '#2b4ed3',
    danger: '#d32b2b',
    success: 'green',
  },
  padding: {
    md: '1.5rem',
  },
};

const ON_SUBMIT = input => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(action('createModel'));
    }, 1200);
  });
};

const ON_COMPLETE = action('onComplete');

const FormWrapper = ({ fields }) => (
  <Form fields={fields} onSubmit={ON_SUBMIT} onComplete={ON_COMPLETE} />
);

const ThemeDecorator = story => (
  <Context.Provider value={theme}>
    <div className="container">{story()}</div>
  </Context.Provider>
);

storiesOf('Form [mode=default]', module)
  .addDecorator(ThemeDecorator)
  .add('Basic Text Input', () => {
    const fields = [
      {
        inputType: 'text',
        name: 'name',
        placeholder: 'Name',
        type: 'string',
      },
    ];
    return <FormWrapper fields={fields} />;
  });
