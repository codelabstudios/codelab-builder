/* eslint-disable @typescript-eslint/no-var-requires */

/**
 * Needs to use require since .eslintrc.yml loads using commonjs
 */
const yaml = require('js-yaml')
const fs = require('fs')
const path = require('path')

let doc

try {
  const file = path.resolve(__dirname, './../.eslintrc.yml')
  doc = yaml.safeLoad(fs.readFileSync(file, 'utf8'))
  // console.log(util.inspect(doc, false, null, true))
} catch (e) {
  console.log(e)
}

module.exports = doc
