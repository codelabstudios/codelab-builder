enum PromiseStatus {
  Pending = 'Pending',
  Success = 'Success',
  Error = 'Error',
}

const wrapPromise = (promise: Promise<any>) => {
  let status = PromiseStatus.Pending
  let result: any
  const suspender = promise.then(
    (r) => {
      status = PromiseStatus.Success
      result = r
    },
    (e) => {
      status = PromiseStatus.Error
      result = e
    },
  )

  return {
    read() {
      if (status === PromiseStatus.Pending) {
        // eslint-disable-next-line @typescript-eslint/no-throw-literal
        throw suspender
      } else if (status === PromiseStatus.Error) {
        throw result
      }
      return result
    },
  }
}

// Map resource name to promise of resource data
type CreateResource = (props: {
  [resourceName: string]: Promise<any>
}) => {
  [resourceName: string]: { read(): any }
}

export const createResource: CreateResource = (props) => {
  return Object.keys(props).reduce((resourceObject, resourceName) => {
    return {
      [resourceName]: wrapPromise(props[resourceName]),
    }
  }, {})
}
