export const getPair = (obj: object) => {
  const data: Array<any> = []
  for (const [key, value] of Object.entries(obj)) {
    data.push([key, value])
  }
  return data[0]
}
