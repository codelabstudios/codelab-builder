import { camelCase, titleCase } from 'voca'
/**
 * E.G.
 *
 * Modal-login--default
 * Notification--error
 *
 * @param block Encapsulates a standalone entity that is meaningful on its own.
 *   While blocks can be nested and interact with each other, semantically they
 *   remain equal; there is no precedence or hierarchy. Holistic entities
 *   without DOM representation (such as controllers or models) can be blocks
 *   as well.
 * @param element Parts of a block and have no standalone meaning. Any element
 *   is semantically tied to its block.
 * @param modifier Flags on blocks or elements. Use them to change appearance,
 *   behavior or state.
 */

/**
 *
 * @param {B} b
 * @param {E} e
 * @param {M} m
 * @return {{bemName: string; bemClass: string}}
 */
export const initBem = <
  B extends string,
  E extends string,
  M extends string
>() => (block: B, element?: E, modifier?: M) => {
  let className = ''

  const b = titleCase(block)
  const e = camelCase(element)
  const m = camelCase(modifier)

  if (b && e && m) {
    className = `${b}-${e}--${m}`
  } else if (b && e) {
    className = `${b}-${e}`
  } else if (b && m) {
    className = `${b}--${m}`
  }

  const cssName = `.${className}`

  return {
    className,
    cssName,
  }
}
