import { inspect } from 'util'

export const consoleLog = (value: any) => {
  console.log(inspect(value, false, null, true))
}
