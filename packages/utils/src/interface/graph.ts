export enum VertexType {
  Model = 'Model',
  Field = 'Field',
  Data = 'Data',
  Content = 'Content',
  Query = 'Query',
  Default = 'Default',
}

export enum EdgeType {
  Field = 'Field',
  Content = 'Content',
  Data = 'Data',
  Query = 'Query',
  Default = 'Default',
}

export enum Relationship {
  OneToOne = 'Relationship 1-1',
  OneToMany = 'Relationship 1-M',
  ManyToOne = 'Relationship M-1',
  ManyToMany = 'Relationship M-M',
}
