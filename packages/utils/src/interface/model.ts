export enum ModelType {
  App = 'App',
  Graph = 'Graph',
  Vertex = 'Vertex',
  VertexType = 'VertexType',
  Edge = 'Edge',
  EdgeType = 'EdgeType',
  Content = 'Content',
}
