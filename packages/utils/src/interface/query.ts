type QueryCondition = {
  fieldLabel: {
    condition: string | number
  }
}

export interface DynamicQuery {
  [model: string]: {
    conditions?: Array<QueryCondition>
  }
}
