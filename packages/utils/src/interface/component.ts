export enum ComponentType {
  Sidebar = 'Sidebar',
  ModalButton = 'ModalButton',
  Form = 'Form',
  Field = 'Field',
  SubmitButton = 'SubmitButton',
  Table = 'Table',
  Menu = 'Menu',
  Link = 'Link',
}
