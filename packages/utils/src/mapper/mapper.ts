import { Relationship } from 'src/interface/graph'

export const mapRelationshipToMongooseType = (relationship: Relationship) => {
  switch (relationship) {
    case Relationship.OneToOne:
    case Relationship.ManyToOne: {
      return 'ObjectId'
    }
    case Relationship.OneToMany:
    case Relationship.ManyToMany: {
      return 'Array'
    }
    default:
      return 'String'
  }
}
