export const getKeyByValue = (map: Map<any, any>, searchValue: any) => {
  for (const [key, value] of map) {
    if (value === searchValue) {
      return key
    }
  }
  return null
}
