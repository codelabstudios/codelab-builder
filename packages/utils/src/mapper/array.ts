import { get, has } from 'lodash'

/**
 * Like lodash get, but works for array items
 *
 *
 * arrayGet({
 *  edgeSrc: [{ label: 'A' }, { label: 'B' }]
 * }, 'edgeSrc.label')
 *
 * ->
 *
 * ['A', 'B']
 *
 */
export const arrayGet = (value: any, key: string): Array<any> => {
  if (!value) {
    return []
  }

  // If key exists as is then return
  if (has(value, key)) {
    return get(value, key)
  }

  const keys = key.split('.')

  // If no more key, return value
  if (keys.length === 1) {
    // If array, go through each array then get value
    if (Array.isArray(value)) {
      return value.map((val) => val[keys[0]])
    }

    return value[keys[0]]
  }

  if (has(value, keys[0])) {
    return arrayGet(value[keys[0]], keys[1])
  }

  return []
}

export const arrayFind = (value: any, key: string): Array<any> => {
  if (!value) {
    return []
  }

  // If key exists as is then return
  if (has(value, key)) {
    return get(value, key)
  }

  const keys = key.split('.')

  // If no more key, return value
  if (keys.length === 1) {
    // If array, go through each array then get value
    if (Array.isArray(value)) {
      return value.map((val) => val[keys[0]])
    }

    return value[keys[0]]
  }

  if (has(value, keys[0])) {
    return arrayGet(value[keys[0]], keys[1])
  }

  return []
}

export const isArray = (array: Array<any> | any): array is Array<any> =>
  !!array.length
