import '@testing-library/cypress/add-commands'

const findButtonByText = (text, options?) => {
  return cy.findByText(text, { exact: true, ...options }).closest('button')
}

Cypress.Commands.add('findButtonByText', findButtonByText)

// https://github.com/cypress-io/add-cypress-custom-command-in-typescript
declare global {
  namespace Cypress {
    interface Chainable {
      findButtonByText: typeof findButtonByText
    }
  }
}
