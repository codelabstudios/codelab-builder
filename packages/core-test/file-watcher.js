const chokidar = require('chokidar');
const path = require('path')

const corePackageFiles = path.resolve(__dirname, '../core/**/!(node_modules|dist)/*.{ts,tsx}')

console.log(corePackageFiles)

// One-liner for current directory
chokidar.watch(corePackageFiles, {
}).on('change', (event, p) => {
  console.log(event, path);
});
