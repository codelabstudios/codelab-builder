import mongoose, { SchemaDefinition } from 'mongoose'

export type SchemaType = {
  [key: string]: typeof mongoose.Schema.Types
}

export const fieldSchemaDefinition: SchemaDefinition = {
  name: {
    type: mongoose.Schema.Types.String,
    require: true,
    unique: true,
  },
  type: {
    type: mongoose.Schema.Types.String,
    require: true,
  },
  required: {
    type: mongoose.Schema.Types.Boolean,
  },
  unique: {
    type: mongoose.Schema.Types.Boolean,
  },
}

export const FieldSchema = new mongoose.Schema(fieldSchemaDefinition)
