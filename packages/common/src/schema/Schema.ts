import { Document, SchemaDefinition } from 'mongoose'

export type TableColumns = Array<{
  key: string
  [key: string]: any
}>

export const schemaTypeToTableColumns = (
  schemaType: SchemaDefinition,
): TableColumns => {
  return Object.keys(schemaType).map((key) => ({
    key,
    title: key,
    dataIndex: key,
  }))
}

// export const

// export const schemaDefinitionToTableColumns = (
//   schemaDefinition: SchemaDefinition,
//   path: string,
// ): TableColumns => {
//   const definition = schemaDefinition[path] as SchemaTypeOpts<any>
//
//   const keys = Object.keys(definition)
//
//   return keys.map((key) => ({
//     key,
//     title: key,
//     dataIndex: key,
//   }))
// }

export const mongooseRecordsToDataSource = (records: Array<Document>) => {
  return records.map((record) => {
    return {
      ...record,
      key: record._id,
    }
  })
}
