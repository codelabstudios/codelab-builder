import * as t from 'io-ts'
import { Validation } from 'io-ts'

const field = t.type({
  name: t.string,
})

export const model = t.type({
  name: t.string,
  fields: t.array(field),
})

export type Model = t.TypeOf<typeof model>

const formFields = t.type({
  name: t.array(t.string),
})

export const formModel = t.type({
  name: t.string,
  fields: formFields,
})

export type FormModel = t.TypeOf<typeof formModel>

// interface ModelBrand {
//   readonly Model: unique symbol // use `unique symbol` here to ensure uniqueness across modules / packages
// }
//
// const MongooseFromForm: C.Codec<Model> = C.make<Model>(
//   {
//     decode: (u: unknown) => {
//       return (u as any)?.name && (u as any)?.fields
//         ? right(u as Model)
//         : left(('error' as unknown) as NonEmptyArray<Tree<string>>)
//     },
//   },
//   { encode: String },
// )

// const modelFromRefinement = fromRefinement<Model>(
//   'Model',
//   (u): u is Model => (u as any)?.name,
// )

// const FormModelBrand = t.brand(t.number, modelFromRefinement, 'Model')

export type ModelFromFormDataC = t.Type<Model, Model, FormModel>

export const mapFormData = (data: FormModel): Model => {
  const fields = Object.entries(data.fields).flatMap(([key, values]) => {
    return values.reduce<Array<any>>((acc, curr) => {
      return [...acc, { [key]: curr }]
    }, [])
  })
  return { ...data, fields }
}

export const ModelFromFormData: ModelFromFormDataC = new t.Type<
  Model, // A
  Model, // O
  FormModel // I
>(
  'ModelFromFormData',
  (u): u is Model => {
    const input = u as FormModel
    return !!(input.name && Array.isArray(input?.fields))
  },
  (input: FormModel, context): Validation<Model> => {
    return input?.name && Array.isArray(input?.fields?.name)
      ? t.success<Model>(mapFormData(input))
      : t.failure<Model>(
          input,
          context,
          'FormModel cannot be transformed to Model',
        )
  },
  (input) => input,
)
