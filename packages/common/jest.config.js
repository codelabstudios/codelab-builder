
module.exports = {
  preset: 'ts-jest',
  transform: {
    '^.+\\.(j|t)sx?$': 'ts-jest',
  },
}
