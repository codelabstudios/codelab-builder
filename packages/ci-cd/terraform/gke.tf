locals {
  network_name = "codelab-us-west2-vpc-1"
  cluster_name = "codelab-us-west2-gke-1"
  subnet_primary_name = "codelab-us-west2-primary"
  subnet_services_name = "codelab-us-west2-services"
  subnet_pods_name = "codelab-us-west2-pods"
}

module "vpc" {
  source = "terraform-google-modules/network/google"
  version = "2.1.1"

  # insert the 3 required variables here
  network_name = local.network_name
  project_id = "codelab-builder-268702"
  subnets = [
    {
      subnet_name = local.subnet_primary_name
      subnet_ip = "/22"
      subnet_region = "us-west2"
    }
  ]

  secondary_ranges = {
    codelab-subnet-01 = [
      {
        range_name = local.subnet_services_name
        ip_cidr_range = "/16"
      },
      {
        range_name = local.subnet_pods_name
        ip_cidr_range = "/20"
      },
    ]
  }

  routes = [
    # Expose VPC to internet
    {
      name = "egress-internet"
      description = "route through IGW to access internet"
      destination_range = "0.0.0.0/0"
      tags = "egress-inet"
      next_hop_internet = "true"
    },
  ]
}

module "kubernetes-engine" {
  source = "terraform-google-modules/kubernetes-engine/google"
  version = "7.3.0"

  # insert the 8 required variables here
  project_id = module.vpc.project_id
  ip_range_pods = local.subnet_pods_name
  ip_range_services = local.subnet_services_name
  name = local.cluster_name
  network = local.network_name
  subnetwork = local.subnet_primary_name

  # Optional
  region = "us-west2"
  zones = [
    "us-west2-a",
    "us-west2-b",
    "us-west2-c"]

  node_pools = [
    {
      name = "codelab-default-node-pool"
      machine_type = "custom-4-6400"
      local_ssd_count = 0
      disk_size_gb = 100
      disk_type = "pd-ssd"
      image_type = "UBUNTU_CONTAINERD"
      auto_upgrade = true
      autoscaling = false
      //      service_account    = "project-service-account@<PROJECT ID>.iam.gserviceaccount.com"
      preemptible = false
      initial_node_count = 1
    },
  ]
}
