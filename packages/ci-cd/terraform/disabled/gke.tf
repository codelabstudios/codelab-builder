resource "google_container_cluster" "primary" {
  name = "codelab-gke-cluster-1"
  project = "codelab-builder-268702"

  # Set regional cluster
  location = "us-west2-a"

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count = 1

  # If this block is not provided, GKE will generate a password for you with the username admin
  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name = "codelab-node-pool-1"
  //  location   = "us-central1"
  cluster = google_container_cluster.primary.name
  project = google_container_cluster.primary.project
  node_count = 1

  node_config {
    machine_type = "custom-4-6400"
  }
}
