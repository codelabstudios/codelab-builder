import { series } from 'gulp'
import { gulpExecSync } from './gulpfile.codegen'

export function kubeCreateSecret(cb) {
  //   const cmd = `create secret generic codelab-docker-reg
  // --from-file=.dockerconfigjson=~/.docker/config.json
  // --type=kubernetes.io/dockerconfigjson`;

  const cmd = `kubectl create -f config/docker-secret.yml`

  return gulpExecSync(cmd, cb)
}

export function kubeDeploy(cb) {
  const cmd = `kubectl apply -k kubernetes/bases`

  return gulpExecSync(cmd, cb)
}

export function komposeConvert(cb) {
  const cmd = `kompose -f docker/docker-compose.dev2.yml -o kubernetes/generated convert`

  return gulpExecSync(cmd, cb)
}

export const kubeUpdate = series(kubeDeploy)
