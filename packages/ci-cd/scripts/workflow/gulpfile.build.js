import { series } from 'gulp'
import { gulpExecSync } from './gulpfile.codegen'

export function buildPackages(cb) {
  // const cmd = 'yarn rimraf packages/*/dist';
  const cmd = ''

  return gulpExecSync(cmd, cb)
}

export const build = series(buildPackages)
