import { series } from 'gulp'
import { gulpExecSync } from './gulpfile.codegen'

export function testIntegration(cb) {
  // const cmd = 'lerna --scope={@codelab/component,} run test --stream';
  const cmd = 'yarn --cwd packages/component run test'

  return gulpExecSync(cmd, cb)
}

export const test = series(testIntegration)
