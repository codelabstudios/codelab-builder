import { exec, execSync } from 'child_process'
import * as findConfig from 'findup-sync'
import { parallel, series, watch } from 'gulp'

require('util').inspect.defaultOptions.depth = null

const del = require('del')

export const envPath = findConfig('.env.dev')

export function gulpExecSync(cmd, cb) {
  execSync(cmd, {
    stdio: 'inherit',
  })

  return cb()
}

export function gulpExec(cmd, cb) {
  const child = exec(cmd, (err, stdout, stderr) => {
    process.stdout.write(err?.message || '')
    // process.stdout.write(stdout);
    process.stdout.write(stderr)
  })

  child.stdout?.on('data', (data) => {
    process.stdout.write(`${data}`)
  })

  return child
}

export function generateCoreTypes(cb) {
  const cmd =
    'yarn graphql-codegen --require dotenv/config ' +
    '--config ./codegen.core.yml ' +
    `dotenv_config_path=${envPath}`

  return gulpExecSync(cmd, cb)
}

export function generateMergedSchema(cb) {
  const cmd =
    'yarn graphql-codegen --require dotenv/config ' +
    '--config ./codegen.schema.yml ' +
    `dotenv_config_path=${envPath}`

  return gulpExecSync(cmd, cb)
}

/**
 * Generates `prisma.graphql` & prisma client
 */
export function generatePrismaClient(cb) {
  const prismaConfig = process.env.PRISMA_CONFIG

  let cmd = `yarn prisma generate --project ${prismaConfig} `

  if (process.env.NODE_ENV === 'env') {
    cmd =
      `yarn prisma generate --project ${prismaConfig} ` +
      `yarn prisma deploy   --project ${prismaConfig}`
  }

  return gulpExecSync(cmd, cb)
}

export function generateGraphqlBinding(cb) {
  const cmd = `yarn graphql --dotenv=${envPath} codegen`

  return gulpExecSync(cmd, cb)
}

export function deleteGeneratedFolders(cb) {
  // Glob patterns https://github.com/micromatch/micromatch
  return del(['packages/{api,core}/**/!(node_modules)/__generated__'], cb)
}

export function clearBabelCache(cb) {
  return del(
    [
      'node_modules/.cache/babel-loader',
      '?packages/?{api,core}/node_modules/.cache/babel-loader',
    ],
    cb,
  )
}

/**
 * Watch
 */
export function watchPrismaClient(cb) {
  watch([process.env.PRISMA_DATAMODEL], generatePrismaClient)
  return cb()
}

export function watchMergedSchema(cb) {
  watch(
    [process.env.SCHEMA_INPUT_LOCAL, process.env.SCHEMA_INPUT_REMOTE],
    generateMergedSchema,
  )
  return cb()
}

export function watchGraphqlBinding(cb) {
  watch([process.env.PRISMA_SCHEMA], generateGraphqlBinding)
  return cb()
}

export function watchCoreTypes(cb) {
  watch(
    [process.env.SCHEMA_INPUT_LOCAL, process.env.SCHEMA_INPUT_REMOTE],
    generateCoreTypes,
  )
  return cb()
}

export const watchCodegen = parallel(
  watchPrismaClient,
  watchMergedSchema,
  watchGraphqlBinding,
  watchCoreTypes,
)

export const codegen = series(
  /**
   * Reset
   */
  deleteGeneratedFolders,
  clearBabelCache,
  /**
   * Generate
   */
  generatePrismaClient,
  generateMergedSchema,
  generateGraphqlBinding,
  generateCoreTypes,
)
