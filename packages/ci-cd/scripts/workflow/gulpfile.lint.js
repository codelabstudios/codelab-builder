import { argv } from 'yargs'
import { gulpExecSync } from './gulpfile.codegen'

export function eslint(cb) {
  const files = argv.files || ''

  const cmd = `ttsc -b packages/eslint-config-codelab && TIMING=1 eslint "**/*.{ts,tsx}" --fix`
  // const cmd = `ttsc -b packages/eslint-config-codelab && eslint "${files}" --fix`;

  console.log(cmd)

  return gulpExecSync(cmd, cb)
}

export function prettify(cb) {
  const files = argv.files || ''

  const cmd = `prettier --config .prettierrc.yml --ignore-path .prettierignore --write "**/*.{ts,tsx,json,graphql,md}"`
  // const cmd = `prettier --config .prettierrc.yml --ignore-path .prettierignore --write "${files}"`;

  console.log(cmd)

  return gulpExecSync(cmd, cb)
}
