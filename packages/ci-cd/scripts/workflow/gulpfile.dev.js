import { parallel, series } from 'gulp'
import { gulpExec, gulpExecSync } from './gulpfile.codegen'
import { projectRootDir } from './gulpfile.config'

export function seed(cb) {
  const cmd = 'curl -s -X POST http://localhost:4000/seeder'

  return gulpExec(cmd, cb)
}

export function watchApi(cb) {
  const cmd = 'yarn --cwd packages/api dev'

  return gulpExec(cmd, cb)
}

export function watchPackages(cb) {
  const cmd =
    'yarn ttsc -b -w packages/api packages/style packages/utils packages/config packages/component packages/eslint-config-codelab packages/core/tsconfig.dev.json'

  return gulpExec(cmd, cb)
}

export function watchCore(cb) {
  const cmd = 'yarn --cwd packages/core dev'

  return gulpExec(cmd, cb)
}

export function startDocker(cb) {
  const cmd = `docker-compose -f ${projectRootDir()}/packages/ci-cd/docker/docker-compose.dev.yml -p codelab-builder up -d`

  return gulpExecSync(cmd, cb)
}

export function reset(cb) {
  const cmd =
    'yarn lerna clean --yes && yarn rimraf packages/*/dist node_modules'

  return gulpExecSync(cmd, cb)
}

export function preinstall(cb) {
  const usingYarn = process?.env?.npm_execpath?.indexOf('yarn') !== -1

  if (!usingYarn) {
    throw Error('Use Yarn instead of NPM')
  }

  return cb()
}

export const devLite = parallel(
  watchApi,
  // watchCodegen,
  watchPackages,
  watchCore,
)

export const predev = series(startDocker)

export const dev = series(predev, devLite)

export const devRestart = series(reset, dev)
