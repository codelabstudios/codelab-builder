import { gulpExecSync } from './gulpfile.codegen'

export function installNPM(cb) {
  const cmd = 'git submodule update --init --recursive && yarn'

  return gulpExecSync(cmd, cb)
}

export function prismaDeploy(cb) {
  const cmd = 'yarn prisma deploy --force'

  return gulpExecSync(cmd, cb)
}
