import { gulpExecSync } from './gulpfile.codegen'

export const prod = cb => {
  const cmd =
    'yarn lerna --scope={@codelab/core,@codelab/api,} run start --parallel'

  return gulpExecSync(cmd, cb)
}
