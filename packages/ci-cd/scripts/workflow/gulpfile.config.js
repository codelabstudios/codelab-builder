import * as findConfig from 'findup-sync'
import * as path from 'path'

export function projectRootDir() {
  const config = findConfig('.env.dev')

  return path.dirname(config)
}
