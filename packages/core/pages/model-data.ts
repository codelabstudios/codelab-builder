export const modalData = {
  type: 'Provider',
  props: {
    ctx: {
      eval: true,
      value:
        'const [visible, setVisible] = this.React.useState(false); return { visible, setVisible }',
    },
    visible: {
      eval: true,
      value: 'return this.visible',
    },
    onOk: {
      eval: true,
      value: 'return () => this.setVisible(false)',
    },
    onCancel: {
      eval: true,
      value: 'return () => this.setVisible(false)',
    },
    onClick: {
      eval: true,
      value: 'return () => this.setVisible(true)',
    },
  },
  renderProps: true,
  children: [
    {
      type: 'Button',
      children: [
        {
          type: 'Text',
          props: {
            value: 'Add Field',
          },
        },
      ],
    },
    {
      type: 'Modal',
      props: {
        title: 'Add Field',
      },
    },
  ],
}

export const createModelForm = {
  type: 'Provider',
  props: {
    onFinish: {
      eval: true,
      value: `return \
        values => { \
          console.log(values); \
          return this.axios.post('api/v1/Field', values).then(({data}) => console.log(data)); \
        } \
      `,
    },
  },
  renderProps: true,
  children: [
    {
      type: 'Form',
      props: {
        name: 'basic',
        initialValues: {},
        onFinish: '',
      },
      children: [
        {
          type: 'Form.Item',
          props: {
            label: 'Name',
            name: 'name',
          },
          children: [
            {
              type: 'Input',
            },
          ],
        },
        {
          type: 'Form.Item',
          props: {
            label: 'Type',
            name: 'type',
          },
          children: [
            {
              type: 'Select',
              props: {
                style: {
                  width: 120,
                },
              },
              children: [
                {
                  type: 'Select.Option',
                  props: {
                    value: 'String',
                  },
                  children: [
                    {
                      type: 'Text',
                      props: {
                        value: 'String',
                      },
                    },
                  ],
                },
                {
                  type: 'Select.Option',
                  props: {
                    value: 'ObjectId',
                  },
                  children: [
                    {
                      type: 'Text',
                      props: {
                        value: 'ObjectId',
                      },
                    },
                  ],
                },
                {
                  type: 'Select.Option',
                  props: {
                    value: 'Number',
                  },
                  children: [
                    {
                      type: 'Text',
                      props: {
                        value: 'Number',
                      },
                    },
                  ],
                },
              ],
            },
          ],
        },
        {
          type: 'Form.Item',
          props: {
            label: 'Required',
            name: 'required',
            valuePropName: 'checked',
          },
          children: [
            {
              type: 'Checkbox',
            },
          ],
        },
        {
          type: 'Form.Item',
          props: {
            label: 'Unique',
            name: 'unique',
            valuePropName: 'checked',
          },
          children: [
            {
              type: 'Checkbox',
            },
          ],
        },
        // {
        //   type: 'Form.Item',
        //   props: {
        //     label: 'username',
        //     name: ['user', 'username'],
        //   },
        //   children: [
        //     {
        //       type: 'Input',
        //     },
        //   ],
        // },
        // {
        //   type: 'Form.Item',
        //   props: {
        //     label: 'email',
        //     name: ['user', 'email'],
        //   },
        //   children: [
        //     {
        //       type: 'Input',
        //     },
        //   ],
        // },
        // {
        //   type: 'Form.List',
        //   renderProps: true,
        //   props: {
        //     name: 'fields',
        //     label: 'Fields',
        //   },
        //   children: [
        //     {
        //       type: 'Form.Item',
        //       props: {
        //         name: 'name',
        //         label: 'Name',
        //       },
        //       children: [
        //         {
        //           type: 'Input',
        //         },
        //       ],
        //     },
        //     {
        //       type: 'Form.Item',
        //       props: {
        //         name: 'type',
        //         label: 'Type',
        //       },
        //       children: [
        //         {
        //           type: 'Input',
        //         },
        //       ],
        //     },
        //   ],
        // },
        {
          type: 'Form.Item',
          children: [
            {
              type: 'Button',
              props: {
                type: 'primary',
                htmlType: 'submit',
              },
              children: [
                {
                  type: 'Text',
                  props: {
                    value: 'Submit',
                  },
                },
              ],
            },
          ],
        },
      ],
    },
  ],
}
