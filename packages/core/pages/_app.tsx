import React from 'react'
import { AppProps } from 'next/app'
import 'antd/dist/antd.css'
import AppLayout from './layout/AppLayout'

const MyApp: React.FC<AppProps> = (props) => {
  const { Component, pageProps } = props

  return (
    <AppLayout>
      <Component {...pageProps} />
    </AppLayout>
  )
}

export default MyApp
