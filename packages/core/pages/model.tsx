import React from 'react'
import { GetStaticProps, InferGetStaticPropsType } from 'next'
import {
  Field,
  fieldSchemaDefinition,
  mongooseRecordsToDataSource,
  schemaTypeToTableColumns,
} from '@codelab/common'
import { Tree } from '@codelab/ui'
import { createModelForm, modalData } from 'pages/model-data'

export const getStaticProps: GetStaticProps = async () => {
  const res = await fetch('http://localhost:4000/api/v1/Field')
  const fields: Array<Field> = await res.json()

  return {
    props: {
      fields,
    },
  }
}

const ModelPage = ({
  fields,
}: InferGetStaticPropsType<typeof getStaticProps>) => {
  const columns = schemaTypeToTableColumns(fieldSchemaDefinition)
  const dataSource: any = mongooseRecordsToDataSource(fields)

  const tableData = {
    type: 'Table',
    props: {
      dataSource,
      columns,
    },
  }

  const Table = Tree.render<any>(tableData)
  const Modal = Tree.render<any>(modalData)
  const CreateModelForm = Tree.render<any>(createModelForm)

  return (
    <>
      <Modal>
        <CreateModelForm />
      </Modal>
      <Table />
    </>
  )
}

export default ModelPage
