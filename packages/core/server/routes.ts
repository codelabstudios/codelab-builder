import Routes from 'next-routes'

export const routes = new Routes()
  .add('index', '/', 'index')
  // .add('app', '/:username/:app/:page')
  // .add('graph', '/graph', 'graph')
  .add('model', '/model/:modelID', 'model')
// .add('content', '/content/:modelID', 'content')
