import 'reflect-metadata'
import next from 'next'
import express from 'express'
import { routes } from './routes'

const app = next({ dev: process.env.NODE_ENV !== 'prod' })
const handler = routes.getRequestHandler(app)

export const bootstrap = async () => {
  app.prepare().then(() => {
    express().use(handler).listen(3000)
  })
}
