// const webpack = require('webpack')
import Dotenv from 'dotenv-webpack'
import findConfig from 'findup-sync'
// const withFonts = require('next-fonts')
import withPlugins from 'next-compose-plugins'
import path from 'path'
import util from 'util'

const withTM = require('next-transpile-modules')(['deepdash-es/standalone'])

// const withPlugins = require('next-compose-plugins')
// const path = require('path')
// const Dotenv = require('dotenv-webpack')
// const findConfig = require('findup-sync')

const nextConfig = {
  webpack(config, { dev, isServer }) {
    // Fixes npm packages that depend on `fs` module
    // if (!isServer) {
    //   config.node = {
    //     fs: 'empty',
    //   }
    // }

    if (dev) {
      /**
       * Source maps
       */
      config.devtool = 'cheap-module-source-map'
      config.module.rules.push({
        test: /\.js$/,
        use: ['source-map-loader'],
        enforce: 'pre',
        exclude: [/.*node_modules.*/],
      })
    }

    /**
     * Apply loader to specific file
     *
     * https://stackoverflow.com/questions/31539061/apply-loader-to-specific-file
     */
    // config.module.rules[0].include.push(
    //   path.resolve(__dirname, '../../node_modules/deepdash-es/standalone.js'),
    // )
    // config.module.rules[0].include.push(/deepdash-es[\\/]standalone\.js$/)

    // config.module.rules.unshift({
    //   test: /deepdash-es[\\/]standalone\.js$/,
    //   use: {
    //     loader: 'babel-loader',
    //     options: {
    //       presets: ['next/babel'],
    //       plugins: ['@babel/plugin-transform-modules-commonjs'],
    //     },
    //   },
    // })

    // Add graphql to extensions
    // config.resolve.extensions.push('.graphql')
    // config.module.rules.push({
    //   test: /\.(graphql|gql)$/,
    //   exclude: /node_modules/,
    //   loader: ['graphql-tag/loader'],
    // })

    // Look in workspace root last
    // config.resolve.modules.push(path.resolve(__dirname, '../../node_modules'))

    // console.log(path.resolve(__dirname, findConfig('.env.dev')))

    /**
     * Add Plugin
     *
     * https://stackoverflow.com/questions/53266814/next-js-pass-node-env-to-client
     */
    config.plugins = [
      ...(config.plugins || []),

      // Read the .env file
      new Dotenv({
        path: path.resolve(__dirname, findConfig('.env.dev')),
        // systemvars: true
      }),
    ]

    // config.externals = [...config.externals]

    // console.log(util.inspect(config, false, null, true /* enable colors */))
    // console.log(config)

    return config
  },
}

module.exports = withPlugins([withTM], nextConfig)
