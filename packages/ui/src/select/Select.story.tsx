import React from 'react'
import { selectData } from 'src/select/Select.data'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Select',
}

export const select = () => {
  const Select = Tree.render(selectData)

  return <Select />
}
