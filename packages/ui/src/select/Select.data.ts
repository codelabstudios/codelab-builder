import { NodeData } from 'src/tree/node/Node.interface'

export const selectData: NodeData = {
  type: 'Select',
  props: {
    defaultValue: 'a',
    style: {
      width: 120,
    },
  },
  children: [
    {
      type: 'Select.Option',
      props: {
        value: 'a',
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'A',
          },
        },
      ],
    },
    {
      type: 'Select.Option',
      props: {
        value: 'b',
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'B',
          },
        },
      ],
    },
    {
      type: 'Select.Option',
      props: {
        value: 'c',
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'C',
          },
        },
      ],
    },
  ],
}
