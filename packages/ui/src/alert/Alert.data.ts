import { NodeData } from 'src/tree/node/Node.interface'
import { AlertProps } from 'src/alert/Alert'

export const alertData: NodeData<AlertProps> = {
  type: 'Alert',
  props: {
    message: 'Success Text',
    description:
      'Detailed description and advice about successful copywriting.',
    type: 'success',
    showIcon: true,
  },
}
