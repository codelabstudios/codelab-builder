import React from 'react'
import { Alert as AntAlert } from 'antd'
import { AlertProps as AntAlertProps } from 'antd/lib/alert'

export interface AlertProps extends AntAlertProps {}

export namespace Alert {
  export const Default = (props: AlertProps) => {
    return <AntAlert {...props} />
  }
}
