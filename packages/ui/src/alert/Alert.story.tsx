import React from 'react'
import { alertData } from 'src/alert/Alert.data'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Alert',
}

export const Default = () => {
  const Alert = Tree.render(alertData)

  return <Alert />
}
