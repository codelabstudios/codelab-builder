import React from 'react'
import { Layout } from 'src/layout/Layout'
import { Layout as AntLayout, Menu } from 'antd'
import { UserOutlined, VideoCameraOutlined } from '@ant-design/icons'

const { Sider } = AntLayout

export default {
  component: Layout,
  title: 'Layout',
}

export const basic = () => {
  const Header = () => (
    <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
      <Menu.Item key="1">nav 1</Menu.Item>
      <Menu.Item key="2">nav 2</Menu.Item>
      <Menu.Item key="3">nav 3</Menu.Item>
    </Menu>
  )

  const Footer = () => <h1>Footer</h1>

  const Content = () => <h1> Content </h1>

  const Sidebar = () => (
    <>
      <div className="logo" />
      <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
        <Menu.Item key="1">
          <UserOutlined />
          <span>Option 1</span>
        </Menu.Item>
        <Menu.Item key="2">
          <VideoCameraOutlined />
          <span>Option 2</span>
        </Menu.Item>
      </Menu>
    </>
  )

  return (
    <Layout
      Header={<Header />}
      Content={<Content />}
      Footer={<Footer />}
      Sidebar={<Sidebar />}
    />
  )
}
