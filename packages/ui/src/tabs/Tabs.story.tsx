import React from 'react'
import { tabsData } from 'src/tabs/Tabs.data'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Tabs',
}

export const Default = () => {
  const Tabs = Tree.render(tabsData)

  return <Tabs />
}
