import { NodeData } from 'src/tree/node/Node.interface'

export const tabsData: NodeData = {
  type: 'Tabs',
  props: {
    defaultActiveKey: 1,
  },
  children: [
    {
      type: 'Tabs.TabPane',
      props: {
        tab: 'Tab 1',
        key: '1',
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'Content of Tab pane 1',
          },
        },
      ],
    },
    {
      type: 'Tabs.TabPane',
      props: {
        tab: 'Tab 2',
        key: '2',
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'Content of Tab pane 2',
          },
        },
      ],
    },
    {
      type: 'Tabs.TabPane',
      props: {
        tab: 'Tab 3',
        key: '3',
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'Content of Tab pane 3',
          },
        },
      ],
    },
  ],
}
