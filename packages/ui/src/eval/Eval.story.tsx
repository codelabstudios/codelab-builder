import React from 'react'
import { Tree } from 'src/tree/Tree'
import { onClickAlert, toggleValue } from 'src/eval/Eval.data'

export default {
  title: 'Eval',
}

export const onClick = () => {
  const Button = Tree.render(onClickAlert)

  return <Button />
}

export const onClickToggle = () => {
  const Button = Tree.render(toggleValue)

  return <Button />
}
