import { NodeData } from 'src/tree/node/Node.interface'

export const onClickAlert: NodeData = {
  type: 'Button',
  props: {
    type: 'primary',
    onClick: {
      type: 'eval',
      value: 'return () => alert("Clicked!")',
    },
  },
  children: [
    {
      type: 'Text',
      props: {
        value: 'Alert',
      },
    },
  ],
}

export const toggleValue: NodeData = {
  type: 'Provider',
  props: {
    ctx: {
      type: 'eval',
      value:
        'const [truthy, toggleTruthy] = this.React.useState(true); return { truthy, toggleTruthy }',
    },
  },
  children: [
    {
      type: 'Button',
      props: {
        type: 'primary',
        onClick: {
          type: 'eval',
          value: 'return () => console.log(this.ctx.truthy)',
        },
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'Toggle',
          },
        },
      ],
    },
    {
      type: 'Html.div',
      children: [
        {
          type: 'Text',
          props: {
            value: 'Test',
          },
        },
      ],
    },
  ],
}
