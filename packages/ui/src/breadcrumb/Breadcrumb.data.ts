import { NodeData } from 'src/tree/node/Node.interface'

export const breadcrumbData: NodeData = {
  type: 'Breadcrumb',
  children: [
    {
      type: 'Breadcrumb.Item',
      props: {
        overlay: {
          type: 'Menu',
          children: [
            {
              type: 'Menu.Item',
              children: [
                {
                  type: 'Text',
                  props: {
                    value: 'Detail 1',
                  },
                },
              ],
            },
            {
              type: 'Menu.Item',
              children: [
                {
                  type: 'Text',
                  props: {
                    value: 'Detail 2',
                  },
                },
              ],
            },
          ],
        },
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'Home',
          },
        },
      ],
    },
    {
      type: 'Breadcrumb.Item',
      children: [
        {
          type: 'Text',
          props: {
            value: 'Detail',
          },
        },
      ],
    },
  ],
}
