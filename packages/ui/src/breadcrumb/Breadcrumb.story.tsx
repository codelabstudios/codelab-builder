import React from 'react'
import { breadcrumbData } from 'src/breadcrumb/Breadcrumb.data'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Breadcrumb',
}

export const Default = () => {
  const Breadcrumb = Tree.render(breadcrumbData)

  return <Breadcrumb />
}
