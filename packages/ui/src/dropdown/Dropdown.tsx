import React from 'react'
import { Dropdown as AntDropdown } from 'antd'
import { DropDownProps as AntDropDownProps } from 'antd/lib/dropdown/dropdown'
import { Tree } from 'src/tree/Tree'
import { RenderProps } from 'src/component/Component.interface'
import { renderPropsChildren } from 'src/props/Props-renderProps'

export interface DropDownProps extends AntDropDownProps {
  overlay: any
}

export namespace Dropdown {
  export const Default = ({
    children,
    overlay,
    ...props
  }: RenderProps<DropDownProps>) => {
    const Overlay = Tree.render(overlay)

    return (
      <AntDropdown overlay={<Overlay />} {...props}>
        {renderPropsChildren(children)}
      </AntDropdown>
    )
  }
}
