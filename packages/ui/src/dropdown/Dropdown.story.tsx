import React from 'react'
import { dropdownData } from 'src/dropdown/Dropdown.data'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Dropdown',
}

export const base = () => {
  const Dropdown = Tree.render(dropdownData)

  return <Dropdown />
}
