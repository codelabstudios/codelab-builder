import { NodeData } from 'src/tree/node/Node.interface'

export const menuData: NodeData = {
  type: 'Menu',
  props: {
    mode: 'inline',
    theme: 'dark',
    style: {
      width: 320,
    },
    defaultOpenKeys: ['sub1'],
  },
  children: [
    {
      type: 'Menu.Item',
      props: {
        key: '1',
        // icon: 'UserOutlined',
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'Option 1',
          },
        },
      ],
    },
    {
      type: 'Menu.SubMenu',
      props: {
        key: 'sub1',
        title: 'Option 2',
        // icon: 'VideoCameraOutlined',
      },
      children: [
        {
          type: 'Menu.Item',
          props: {
            key: '2',
            // icon: 'UserOutlined',
          },
          children: [
            {
              type: 'Text',
              props: {
                value: 'Sub 1',
              },
            },
          ],
        },
        {
          type: 'Menu.Item',
          props: {
            key: '3',
            // icon: 'UserOutlined',
          },
          children: [
            {
              type: 'Text',
              props: {
                value: 'Sub 2',
              },
            },
          ],
        },
      ],
    },
  ],
}
