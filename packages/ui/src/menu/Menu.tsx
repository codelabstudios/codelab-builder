import React from 'react'
import { Menu as AntMenu } from 'antd'
import { MenuProps as AntMenuProps } from 'antd/lib/menu'
import { MenuItemProps as AntMenuItemProps } from 'antd/lib/menu/MenuItem'
import { SubMenuProps as AntSubMenuProps } from 'antd/lib/menu/SubMenu'
import { RenderProps } from 'src/component/Component.interface'
import { renderPropsChildren } from 'src/props/Props-renderProps'

export interface MenuProps extends AntMenuProps {}
export interface MenuItemProps extends AntMenuItemProps, React.Attributes {
  icon?: string
}
export interface SubMenuProps extends AntSubMenuProps, React.Attributes {
  text: string
  icon?: string
}

export namespace Menu {
  export const Default = ({ children, ...props }: RenderProps<MenuProps>) => {
    const { mode, theme, defaultOpenKeys, ...rest } = props

    return (
      <AntMenu
        mode={mode}
        theme={theme}
        defaultOpenKeys={defaultOpenKeys}
        {...rest}
      >
        {renderPropsChildren(children)}
      </AntMenu>
    )
  }

  export const Item = ({ children, ...props }: RenderProps<MenuItemProps>) => {
    const { key, icon, ...rest } = props

    return (
      <AntMenu.Item key={key} {...rest}>
        {renderPropsChildren(children)}
      </AntMenu.Item>
    )
  }

  export const SubMenu = ({
    children,
    ...props
  }: RenderProps<SubMenuProps>) => {
    console.log(props)

    const { text, key, icon, ...rest } = props

    return (
      <AntMenu.SubMenu key={key} {...rest} title={<span>{text}</span>}>
        {renderPropsChildren(children)}
      </AntMenu.SubMenu>
    )
  }
}
