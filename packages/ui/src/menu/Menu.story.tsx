import React from 'react'
import { menuData } from 'src/menu/Menu.data'
import { Tree } from 'src/tree/Tree'
import { Menu as AntMenu } from 'antd'
import {
  AppstoreOutlined,
  MailOutlined,
  SettingOutlined,
} from '@ant-design/icons'

const { SubMenu } = AntMenu

export default {
  title: 'Menu',
}

export const Default = () => {
  const Menu = Tree.render(menuData)

  return <Menu />
}

const MenuItem = (props: any) => {
  console.log(props)
  return (
    <AntMenu.Item key="mail" icon={<MailOutlined />} {...props}>
      Navigation One
    </AntMenu.Item>
  )
}

export const antd = () => {
  return (
    <AntMenu mode="horizontal">
      {() => {
        return <MenuItem />
      }}
      <AntMenu.Item key="app" disabled icon={<AppstoreOutlined />}>
        Navigation Two
      </AntMenu.Item>
      <SubMenu icon={<SettingOutlined />} title="Navigation Three - Submenu">
        <AntMenu.ItemGroup title="Item 1">
          <AntMenu.Item key="setting:1">Option 1</AntMenu.Item>
          <AntMenu.Item key="setting:2">Option 2</AntMenu.Item>
        </AntMenu.ItemGroup>
        <AntMenu.ItemGroup title="Item 2">
          <AntMenu.Item key="setting:3">Option 3</AntMenu.Item>
          <AntMenu.Item key="setting:4">Option 4</AntMenu.Item>
        </AntMenu.ItemGroup>
      </SubMenu>
      <AntMenu.Item key="alipay">
        <a href="https://ant.design" target="_blank" rel="noopener noreferrer">
          Navigation Four - Link
        </a>
      </AntMenu.Item>
    </AntMenu>
  )
}
