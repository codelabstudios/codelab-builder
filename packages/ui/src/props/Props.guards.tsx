import { Props, PropValue } from 'src/component/Component.interface'

export const isEvalPropValue = (
  propValue: Props[keyof Props],
): propValue is PropValue => {
  return typeof propValue === 'object' && propValue?.eval
}

export const isRenderPropValue = (
  propValue: Props[keyof Props],
): propValue is PropValue => {
  return typeof propValue === 'object' && !!propValue?.renderProps
}
