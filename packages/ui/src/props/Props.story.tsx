import React from 'react'
import { Tree } from 'src/tree'
import { renderPropsData } from 'src/props/Props-renderProps.data'

export default {
  title: 'Props',
}

export const Default = () => {
  const Component = Tree.render(renderPropsData)

  return <Component />
}
