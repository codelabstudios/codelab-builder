import { PropFilterPredicate } from 'src/event/Event-mouse'
import { pickBy } from 'lodash'
import { anyPass, curry } from 'ramda'
import { Props } from 'src/component/Component.interface'

export const isValidKey = curry<PropFilterPredicate>((propKeys, value, key) => {
  return [...propKeys, 'children'].includes(key as any)
})

export const propsFilter = curry((propKeys: Array<string>, props: Props) => {
  return pickBy(props, anyPass([isValidKey(propKeys) as any]))
})
