import {
  Props,
  PropTypes,
  RenderPropsChildren,
} from 'src/component/Component.interface'
import React from 'react'
import { reduce } from 'lodash'
import { isRenderPropValue } from 'src/props/Props.guards'

/**
 * Remove render props
 */
export function filterRenderProps(props: Props): Props {
  return reduce<Props, Props>(
    props,
    (prop: Props, propValue: Props[keyof Props], propKey: keyof Props) => {
      if (isRenderPropValue(propValue)) {
        return {
          ...prop,
          [propKey]: propValue,
        }
      }

      return {
        ...prop,
      }
    },
    {},
  )
}

export function renderPropsChildren<P extends Props>(
  children: RenderPropsChildren<P> = [],
  props: P = {} as P,
) {
  const childrenArray = Array.isArray(children) ? children : [children]

  return (
    <>
      {childrenArray.map((child: any, index) => {
        return <React.Fragment key={index}>{child(props)}</React.Fragment>
      })}
    </>
  )
}
