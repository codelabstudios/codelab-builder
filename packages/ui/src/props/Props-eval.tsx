import { Props, PropTypes, PropValue } from 'src/component/Component.interface'
import { reduce } from 'lodash'
import { isEvalPropValue } from 'src/props/Props.guards'
import React from 'react'
import axios from 'axios'

export const evalPropValue = (propValue: PropValue, ctx?: any): Function => {
  return new Function(propValue.value).call(ctx)
}

export function evalProps<P extends Props = Props>(props: P, ctx?: any) {
  return reduce<P, Props>(
    props,
    (
      evaluatedProp: Props,
      propValue: Props[keyof Props],
      propKey: keyof Props,
    ) => ({
      ...evaluatedProp,
      [propKey]: isEvalPropValue(propValue)
        ? evalPropValue(propValue, ctx)
        : propValue,
    }),
    {},
  )
}

/**
 * Allows us to build ctx & pass into props without needing a parent & child component
 */
export function evalPropsWithContext(props: Props): Props {
  const { ctx, ...restProps } = props

  if (!ctx) {
    return props
  }

  // We first take ctx from current props & eval that
  const { ctx: context } = evalProps({ ctx }, { React, axios })

  // Then pass the ctx into rest of props
  return {
    ...evalProps(restProps, context),
  }
}
