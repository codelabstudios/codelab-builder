import React from 'react'
import { Props } from 'src/component/Component.interface'
import { filterRenderProps } from 'src/props/Props-renderProps'
import { renderPropsData } from 'src/props/Props-renderProps.data'
import { Tree } from 'src/tree'
import { mount } from 'enzyme'
import { omit } from 'lodash'

describe('RenderProps', () => {
  it('filters props for renderProps', () => {
    const props: Props = {
      onHover: {
        someAction: '',
      },
      onClick: {
        renderProps: true,
        someAction: '',
      },
    }
    const expectedProps: Props = {
      onClick: {
        renderProps: true,
        someAction: '',
      },
    }

    const renderProps = filterRenderProps(props)

    expect(renderProps).toStrictEqual(expectedProps)
  })

  it('can pass props from parent to child', () => {
    const Component = Tree.render(renderPropsData)
    const wrapper = mount(<Component />)

    const parent = wrapper.find('div').get(0)
    const child = wrapper.find('div').get(1)

    // Test parent component's props
    const actualParentProps = omit(parent.props, 'children')

    expect(actualParentProps).toEqual({
      visibility: '',
      parentprops: {
        renderProps: true,
      },
    })

    const actualChildProps = omit(child.props, 'children')

    expect(actualChildProps).toEqual({
      parentprops: { renderProps: true },
      childprops: {},
    })
  })
})
