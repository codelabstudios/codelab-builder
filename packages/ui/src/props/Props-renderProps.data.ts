import { NodeData } from 'src/tree/node/Node.interface'

export const renderPropsData: NodeData = {
  type: 'Html.div',
  props: {
    visibility: '',
    parentprops: {
      renderProps: true,
    },
  },
  children: [
    {
      type: 'Html.div',
      props: {
        childprops: {},
      },
    },
  ],
}
