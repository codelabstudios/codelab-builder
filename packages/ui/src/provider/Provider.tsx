import React, { PropsWithChildren } from 'react'
import axios from 'axios'

axios.create({
  baseURL: 'https://localhost:4000/',
  // timeout: 1000,
  // headers: { 'X-Custom-Header': 'foobar' },
})

export namespace Provider {
  export const Default: React.FC<PropsWithChildren<any>> = ({
    children,
    ...props
  }) => {
    return (
      <>
        {(Array.isArray(children) ? children : [children]).map(
          (child: React.ReactElement, index: number) => (
            <React.Fragment key={index}>
              {React.cloneElement(child, { ...props })}
            </React.Fragment>
          ),
        )}
      </>
    )
  }
}
