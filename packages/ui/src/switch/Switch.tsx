import React from 'react'
import { Switch as AntSwitch } from 'antd'
import { SwitchProps as AntSwitchProps } from 'antd/lib/switch'

export interface SwitchProps extends AntSwitchProps {}

export namespace Switch {
  export const Default: React.FC<SwitchProps> = (props) => {
    return <AntSwitch {...props} />
  }
}
