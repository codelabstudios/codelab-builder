import React from 'react'
import { switchData } from 'src/switch/Switch.data'
import { Switch as AntSwitch } from 'antd'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Switch',
}

export const base = () => {
  const Switch = Tree.render(switchData)

  return <Switch />
}

export const antd = () => {
  return (
    <AntSwitch checkedChildren="On" unCheckedChildren="Off" defaultChecked />
  )
}
