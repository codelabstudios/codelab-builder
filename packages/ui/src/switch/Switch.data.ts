import { NodeData } from 'src/tree/node/Node.interface'

export const switchData: NodeData = {
  type: 'Switch',
  props: {
    checkedChildren: 'On',
    unCheckedChildren: 'Off',
  },
}
