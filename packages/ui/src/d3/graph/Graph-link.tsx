import { g, LinkAttribute } from 'src/d3/graph/Graph-variables'
import { BaseType, ValueFn } from 'd3-selection'
import { LinkType } from 'src/d3/graph/Graph.interface'

// type GetLinkAttribute = (
//   attr: keyof LinkAttribute,
// ) => (d: TEdge) => LinkAttribute[keyof LinkAttribute];

type GetLinkAttribute<
  GElement extends BaseType,
  Datum,
  Results = string | number
> = (attr: keyof LinkAttribute) => ValueFn<GElement, Datum, Results>

export const linkAttribute: GetLinkAttribute<any, any> = (attr) => (d) => {
  const typename = d?.type?.name as LinkType
  return g.link[typename]?.[attr] || g.link[LinkType.Default][attr]
}
