import * as d3 from 'd3'
import { g } from 'src/d3/graph/Graph-variables'
import { nonActiveLinks, nonActiveNodes } from 'src/d3/graph/Graph-filters'
import { nodeAttribute } from 'src/d3/graph/Graph-node'
import { linkAttribute } from 'src/d3/graph/Graph-link'
import {
  clearTempLink,
  drawingTempLink,
} from 'src/d3/graph/Graph-updatePattern'

type DomHandler<T> = (node: T, i: number, nodes: ArrayLike<T>) => any
type DomHandlerHOC<T, M> = (mutation?: M) => DomHandler<T>

const deactivateNodes = (nodes: any) =>
  d3
    .selectAll(nodes)
    .filter(nonActiveNodes)
    .attr('fill', nodeAttribute('color'))

const deactivateAllNodes = () =>
  d3.selectAll('g.Vertex-group').attr('fill', nodeAttribute('color'))

const deactivateLinks = (links: any) => {
  d3.selectAll(links)
    .filter(nonActiveLinks)
    .select('path')
    .attr('stroke', linkAttribute('color'))

  d3.selectAll(links)
    .filter(nonActiveLinks)
    .select('text')
    .attr('fill', linkAttribute('color'))

  d3.selectAll('.arrow')
    .filter(nonActiveLinks)
    .select('path')
    .attr('fill', linkAttribute('color'))
}

const deactivateAllLinks = () => {
  d3.selectAll('g.Edge-group')
    .select('path')
    .attr('stroke', linkAttribute('color'))

  d3.selectAll('text.Edge-label')
    .select('text')
    .attr('fill', linkAttribute('color'))

  d3.selectAll('.arrow').select('path').attr('fill', linkAttribute('color'))
}

/**
 * Node handlers
 */
export const handleClickNode: DomHandlerHOC<any, any> = ({
  notifyMediator,
  setSelectedVertex,
}) => (node, i, nodes) => {
  const modelID = node.label === '+' ? '' : node.id

  return Promise.all([
    /**
     * if label === '+', then trigger create event
     */
    // setSelectedVertex(modelID),
  ]).then(() => {
    // Fill active node
    g.activeNode = node
    d3.select(nodes[i]).attr('fill', g.activeNodeColor)
    deactivateNodes(nodes)
    deactivateAllLinks()
  })
}

export const handleMouseoverNode: DomHandler<any> = (node, i, nodes) => {
  d3.select(nodes[i]).attr('fill', g.activeNodeColor)
}

export const handleMouseoutNode: DomHandler<any> = (node, i, nodes) => {
  deactivateNodes(nodes)
}

// Drag & Drop Handlers
export const handleDragStart: DomHandler<any> = (node, i, nodes) => {
  console.log('handleDragStart')
  g.activeNode = node
  clearTempLink()
}

export const handleDragNode: DomHandler<any> = (node, i, nodes) => {
  console.log('handleDragNode')
  const {
    sourceEvent: {
      srcElement: { __data__: target },
    },
    x,
    y,
  } = d3.event

  console.log(target)

  const targetIdx =
    target && target.id !== g.activeNode.id
      ? Array.from(nodes).findIndex((d) => d.__data__.id === target.id)
      : -1

  if (targetIdx > -1) {
    const [targetNode] = d3.select(nodes[targetIdx]).data()
    drawingTempLink(node, targetNode)
  } else {
    drawingTempLink(node, { x, y })
  }
}

export const handleDragEndNode: DomHandlerHOC<any, any> = ({
  setNewEdge,
  notifyMediator,
}) => (node, i, nodes) => {
  console.log('handleDragEndNode')
  const {
    sourceEvent: {
      srcElement: { __data__: target },
    },
  } = d3.event
  const targetIdx =
    target && target.id !== g.activeNode.id
      ? Array.from(nodes).findIndex((d) => d.__data__.id === target.id)
      : -1

  if (targetIdx > -1) {
    const [targetNode] = d3.select(nodes[targetIdx]).data()
    drawingTempLink(node, targetNode)
    setNewEdge(g.activeNode.id, target.id)
  } else {
    clearTempLink()
  }
}

/**
 * Link handlers
 */

// export const handleMouse
export const handleMouseoverLink: DomHandler<any> = (link, i, links) => {
  d3.select(links[i]).select('path').attr('stroke', g.activeLinkColor)
  d3.select(`#arrow_${link.id}`).select('path').attr('fill', g.activeLinkColor)
}

export const handleMouseoutLink: DomHandler<any> = (link, i, links) => {
  deactivateLinks(links)
}

export const handleClickLink: DomHandlerHOC<any, any> = ({
  notifyMediator,
  setSelectedEdge,
}) => (link, i, links) =>
  Promise.all([setSelectedEdge(link.id)]).then(() => {
    g.activeLink = link
    d3.select(links[i]).select('path').attr('stroke', g.activeLinkColor)
    d3.select(`#arrow_${link.id}`)
      .select('path')
      .attr('fill', g.activeLinkColor)
    deactivateLinks(links)
    deactivateAllNodes()
  })
