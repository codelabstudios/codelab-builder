import { g } from 'src/d3/graph/Graph-variables'
import * as d3 from 'd3'
import { drag } from 'd3'
import {
  handleClickLink,
  handleClickNode,
  handleDragEndNode,
  handleDragNode,
  handleDragStart,
  handleMouseoutLink,
  handleMouseoutNode,
  handleMouseoverLink,
  handleMouseoverNode,
} from 'src/d3/graph/Graph-domHandlers'
import { nodeAttribute } from 'src/d3/graph/Graph-node'
import { D3Link, D3Node } from 'src/d3/graph/Graph.interface'

/**
 * D3 update methods
 */
export const enterNodes = (selection: any, d3Hooks: any) => {
  selection.append('circle').attr('r', nodeAttribute('radius'))
  selection.attr('fill', nodeAttribute('color'))
  selection.append('text').text((d: any) => d.label)
  /**
   * circle.vertex
   */
  selection
    .attr('class', 'Vertex-group')
    .select('circle')
    .attr('class', 'Vertex')
    .style('cursor', 'pointer')
  /**
   * Add dom handlers
   */
  selection
    .on('click', handleClickNode(d3Hooks).bind(selection))
    .on('mouseover', handleMouseoverNode.bind(selection))
    .on('mouseout', handleMouseoutNode.bind(selection))

  /**
   * Drag & Drop
   */
  selection.call(
    drag<any, any>()
      .on('start', handleDragStart.bind(selection))
      .on('drag', handleDragNode.bind(selection))
      .on('end', handleDragEndNode(d3Hooks).bind(selection)),
  )
}

export const updateNodes = (selection: any) => {
  selection
    .select('circle')
    .attr('cx', (d: any) => d.x)
    .attr('cy', (d: any) => d.y)
    .attr('r', nodeAttribute('radius'))

  selection
    .select('text')
    .text((d: D3Node) => d?.label ?? d.id)
    .attr('transform', (d: any) => {
      const x = d.x || g.vertexRadius
      const y = (d.y || -g.vertexRadius) + g.labelOffset

      return `translate(${x - g.vertexRadius},${y + g.vertexRadius})`
    })
}

export const enterLinks = (selection: any, d3Hooks: any, links: any) => {
  selection.attr('class', 'Edge-group')

  /**
   * Add link between vertices
   */
  selection
    .append('path')
    .attr('class', 'Edge-line')
    .attr('id', (d: D3Link) => `edge_path_${d.id}`)
    .attr('stroke', 'gray')
    .attr('stroke-width', '1px')
    .attr('fill', 'none')
    .attr('marker-mid', (d: any) => `url(#arrow_${d.id})`)
    .style('cursor', 'pointer')

  /**
   * Append thicker path for easier mouse click
   */
  selection
    .append('path')
    .attr('class', 'Edge-hover')
    .attr('stroke', 'transparent')
    .attr('stroke-width', '6px')
    .attr('fill', 'none')
    .style('cursor', 'pointer')

  /**
   * Add label text
   */
  selection
    .append('text')
    .attr('class', 'Edge-label')
    .attr('text-anchor', 'middle')
    .attr('x', 0)
    .attr('dy', 16)
    .attr('fill', 'gray')
    .attr('font-size', 12)
    .style('user-select', 'none')
    .style('cursor', 'pointer')
    .append('textPath')
    .attr('startOffset', '50%')
    .attr('xlink:href', (d: any) => `#edge_path_${d.id}`)
    .text((d: any) => d.label)

  /**
   * Add dom handlers
   */
  selection
    .on('click', handleClickLink(d3Hooks).bind(selection))
    .on('mouseover', handleMouseoverLink.bind(selection))
    .on('mouseout', handleMouseoutLink.bind(selection))
}

export const updateLinks = (selection: any, links = []) => {
  const addEdge = (d: any) => {
    // TODO
    // if (!has(d, 'target.id')) return ''

    const divider = d.biDirection ? 1 / 2 : 0

    const [dX, dY] = [d.target.x - d.source.x, d.target.y - d.source.y]

    const [x1, y1, x2, y2] = [
      d.source.x + dX * divider,
      d.source.y + dY * divider,
      d.target.x,
      d.target.y,
    ]

    const dx = x2 - x1
    const dy = y2 - y1

    return `M${x1},${y1} L${x1 + dx / 2},${y1 + dy / 2}  L${x2},${y2}`
  }

  const updateEdgeLabel = (d: any, i: number, elements: Array<any>) => {
    if (d?.target?.id) return ''

    const { x, y, width, height } = elements[i].getBBox()
    const rotate =
      d.target.x < d.source.x
        ? `rotate(180 ${x + width / 2} ${y + height / 2})`
        : 'rotate(0)'
    return rotate
  }

  selection.select('path.Edge-line').attr('d', addEdge)
  selection.select('path.Edge-hover').attr('d', addEdge)
  selection
    .select('text.Edge-label')
    .attr('transform', updateEdgeLabel.bind(selection))
}

export const defineMarkers = (selection: any) => {
  selection
    .attr('class', 'arrow')
    .attr('id', (d: any) => `arrow_${d.id}`)
    .attr('viewBox', '0 0 12 16')
    .attr('refX', 6)
    .attr('refY', 8)
    .attr('markerWidth', 12)
    .attr('markerHeight', 16)
    .attr('orient', 'auto')
    .append('svg:path')
    .attr('d', 'M2,2 L14,8 L2,14 L8,8 L2,2')
    .attr('fill', 'gray')
}

export const drawingTempLink = (source: any, target: any) => {
  const [x1, y1, x2, y2] = [source.x, source.y, target.x, target.y]
  const dx = x2 - x1
  const dy = y2 - y1
  const dPath = `M${x1},${y1} L${x1 + dx / 2},${y1 + dy / 2}  L${x2},${y2}`
  d3.select('path.temp-link').attr('d', dPath)
  return dPath
}

export const clearTempLink = () => {
  d3.selectAll('path.temp-link').attr('d', '')
}

/**
 * Use function for this context
 */
export function ticked(this: any): any {
  updateNodes(this.d3Nodes)
  updateLinks(this.d3Links)
}
