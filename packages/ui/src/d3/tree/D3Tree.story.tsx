import React from 'react'
import { D3Tree } from 'src/d3/tree/D3Tree'
import { d3TreeData } from 'src/d3/tree/D3-tree.data'

export default {
  component: D3Tree,
  title: 'D3Tree',
}

export const basic = () => {
  return <D3Tree data={d3TreeData} />
}
