import { RenderProps } from 'src/component/Component.interface'
import { renderPropsChildren } from 'src/props/Props-renderProps'
import React from 'react'

export interface HtmlProps {}

export interface TextProps {
  value: string
}

export namespace Html {
  export function Text({ value }: TextProps) {
    return <>{value}</>
  }

  export function Div({ children, ref, ...props }: RenderProps<HtmlProps>) {
    return (
      <div ref={ref} {...props}>
        {renderPropsChildren(children, props)}
      </div>
    )
  }
}
