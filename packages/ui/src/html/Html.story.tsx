import React from 'react'
import { Tree } from 'src/tree'
import { divData } from './Html.data'

export default {
  title: 'Html',
}

export const div = () => {
  const Div = Tree.render(divData)

  return <Div />
}
