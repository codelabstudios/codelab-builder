import { NodeData } from 'src/tree/node/Node.interface'

export const divData: NodeData = {
  type: 'Html.div',
  props: {
    visibility: '',
    parentprops: {
      renderProps: true,
    },
  },
  children: [
    {
      type: 'Html.div',
      props: {
        childprops: {},
      },
    },
  ],
}
