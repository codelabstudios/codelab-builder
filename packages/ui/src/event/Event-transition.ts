import { TransitionEventKeys } from 'src/event/Event.interface'

export const transitionEventHandlerKeys: Array<TransitionEventKeys> = [
  'onTransitionEnd',
  'onTransitionEndCapture',
]
