import { KeyboardEventKeys } from 'src/event/Event.interface'

export const keyboardEventHandlerKeys: Array<KeyboardEventKeys> = [
  'onKeyDown',
  'onKeyDownCapture',
  'onKeyPress',
  'onKeyPressCapture',
  'onKeyUp',
  'onKeyUpCapture',
]
