import { FormEventKeys } from 'src/event/Event.interface'

export const formEventHandlerKeys: Array<FormEventKeys> = [
  'onChange',
  'onChangeCapture',
  'onBeforeInput',
  'onBeforeInputCapture',
  'onInput',
  'onInputCapture',
  'onReset',
  'onResetCapture',
  'onSubmit',
  'onSubmitCapture',
  'onInvalid',
  'onInvalidCapture',
]
