import { ImageEventKeys } from 'src/event/Event.interface'

export const imageEventHandlerKeys: Array<ImageEventKeys> = [
  'onLoad',
  'onLoadCapture',
  'onError',
  'onErrorCapture',
]
