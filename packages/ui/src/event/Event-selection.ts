import { SelectionEventKeys } from 'src/event/Event.interface'

export const selectionEventHandlerKeys: Array<SelectionEventKeys> = [
  'onSelect',
  'onSelectCapture',
]
