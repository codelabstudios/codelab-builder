import { CompositionEventKeys } from 'src/event/Event.interface'

export const compositionEventHandlerKeys: Array<CompositionEventKeys> = [
  'onCompositionEnd',
  'onCompositionEndCapture',
  'onCompositionStart',
  'onCompositionStartCapture',
  'onCompositionUpdate',
  'onCompositionUpdateCapture',
]
