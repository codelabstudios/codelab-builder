import { AnimationEventKeys } from 'src/event/Event.interface'

export const animationEventHandlerKeys: Array<AnimationEventKeys> = [
  'onAnimationStart',
  'onAnimationStartCapture',
  'onAnimationEnd',
  'onAnimationEndCapture',
  'onAnimationIteration',
  'onAnimationIterationCapture',
]
