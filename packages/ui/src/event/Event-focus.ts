import { FocusEventKeys } from 'src/event/Event.interface'

export const focusEventHandlerKeys: Array<FocusEventKeys> = [
  'onFocus',
  'onFocusCapture',
  'onBlur',
  'onBlurCapture',
]
