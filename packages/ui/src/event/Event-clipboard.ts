import { ClipboardEventKeys } from 'src/event/Event.interface'

export const clipboardEventHandlerKeys: Array<ClipboardEventKeys> = [
  'onCopy',
  'onCopyCapture',
  'onCut',
  'onCutCapture',
  'onPaste',
  'onPasteCapture',
]
