import { UiEventKeys } from 'src/event/Event.interface'

export const uiEventHandlerKeys: Array<UiEventKeys> = [
  'onScroll',
  'onScrollCapture',
]
