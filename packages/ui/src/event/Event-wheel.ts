import { WheelEventKeys } from 'src/event/Event.interface'

export const wheelEventHandlerKeys: Array<WheelEventKeys> = [
  'onWheel',
  'onWheelCapture',
]
