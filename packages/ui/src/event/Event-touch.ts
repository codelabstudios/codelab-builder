import { TouchEventKeys } from 'src/event/Event.interface'

export const touchEventHandlerKeys: Array<TouchEventKeys> = [
  'onTouchCancel',
  'onTouchCancelCapture',
  'onTouchEnd',
  'onTouchEndCapture',
  'onTouchMove',
  'onTouchMoveCapture',
  'onTouchStart',
  'onTouchStartCapture',
]
