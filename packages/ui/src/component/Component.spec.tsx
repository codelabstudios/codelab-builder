import { componentData } from 'src/tree/Tree-component.data'
import React from 'react'
import { render } from '@testing-library/react'
import { Tree } from 'src/tree/Tree'

describe('Component tree', () => {
  it('renders a component tree', () => {
    const Component = Tree.render(componentData)

    const { getByText } = render(<Component />)
    const root = getByText('root')

    expect(root?.childNodes[1]).toHaveTextContent('A')
    // expect(root?.nextSibling?.firstChild?.firstChild).toHaveTextContent('C')
    // expect(
    //   root?.nextSibling?.firstChild?.firstChild?.nextSibling,
    // ).toHaveTextContent('D')
  })
})
