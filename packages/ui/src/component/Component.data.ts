import { NodeData } from 'src/tree/node/Node.interface'

export const componentPropsData: NodeData = {
  id: 'root',
  props: {
    name: 'root',
  },
  children: [
    {
      id: 'A',
      props: {
        name: 'A',
      },
      children: [
        {
          id: 'B',
          props: {
            name: 'B',
          },
          children: [
            {
              id: 'C',
              props: {
                name: 'C',
              },
            },
            {
              id: 'D',
              props: {
                name: 'D',
              },
            },
          ],
        },
      ],
    },
    {
      id: 'E',
      props: {
        name: 'E',
      },
      children: [
        {
          id: 'F',
          props: {
            name: 'F',
          },
        },
        {
          id: 'G',
          props: {
            name: 'G',
          },
        },
        {
          id: 'H',
          props: {
            name: 'H',
          },
        },
      ],
    },
  ],
}
