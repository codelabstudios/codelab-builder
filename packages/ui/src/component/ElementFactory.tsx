import {
  ComponentProps,
  Props,
  PropTypes,
} from 'src/component/Component.interface'
import { Node } from 'src/tree/node/Node'
import React, {
  Attributes,
  ComponentClass,
  FunctionComponent,
  PureComponent,
} from 'react'
import {
  Alert,
  Button,
  Card,
  Checkbox,
  Dropdown,
  Form,
  Input,
  Menu,
  Modal,
  Radio,
  Select,
  Slider,
  Space,
  Switch,
  Table,
  Tabs,
  Typography,
  Breadcrumb,
} from 'antd'
import { Html } from 'src/html/Html'
import { Tree } from 'src/tree/Tree'
import { mapValuesDeep } from 'deepdash/standalone'
import { Grid } from 'src/grid/Grid'
import { Provider } from 'src/provider/Provider'
import { compose } from 'ramda'
import { propsFilter } from 'src/props/Props-filter'
import { mouseEventHandlerKeys } from 'src/event/Event-mouse'
import { buttonPropKeys } from 'src/button/Button.data'
import { has } from 'lodash'

export type PropsFilter<P extends ComponentProps> = (props: P) => P

type ElementParameters<P> = [
  FunctionComponent<P> | ComponentClass<P> | string,
  Attributes & P,
  PropsFilter<P>?,
]

function withFilters(
  filterFn: (props: Props) => Props,
  Component: FunctionComponent<Props>,
): FunctionComponent<Props> {
  return compose((props: Props) => <Component {...props} />, filterFn)
}

export const elementParameterFactory = <P extends ComponentProps>(
  node: Node<P>,
): ElementParameters<Props> => {
  const { type, props } = node

  switch (type) {
    case 'React.Fragment':
      return [React.Fragment, props]
    case 'Html.div':
      return ['div', props]
    case 'Text':
      return [Html.Text as any, props]
    case 'Menu':
      return [Menu as any, props]
    case 'Menu.Item':
      return [Menu.Item as any, props]
    case 'Menu.SubMenu':
      return [Menu.SubMenu as any, props]
    case 'Card':
      return [Card, props]
    case 'Card.Grid':
      return [Card.Grid, props]
    case 'Card.Meta':
      return [Card.Meta, props]
    case 'Alert':
      return [Alert as any, props]
    case 'Button':
      return [
        withFilters(
          propsFilter([...mouseEventHandlerKeys, ...buttonPropKeys]),
          Button,
        ),
        props,
      ]
    case 'Dropdown': {
      const overlayProp = (props as any)?.overlay
      const Overlay = overlayProp && Tree.render<any>(overlayProp)
      return [Dropdown as any, { ...props, overlay: Overlay && <Overlay /> }]
    }
    case 'Table':
      const tableProps = mapValuesDeep(
        props,
        (value: Props[keyof Props], key: keyof Props) => {
          if (key === 'render') {
            console.log(value)
            const Render = Tree.render(value)
            return (text: string, record: any) => <Render {...record} />
          }
          return value
        },
      )

      console.log(tableProps)

      return [Table, tableProps]
    case 'Form':
      return [Form, props]
    case 'Form.Item':
      return [Form.Item as any, props]
    case 'Checkbox':
      return [Checkbox as any, props]
    case 'Input': // can't have children
      return [Input as any, props]
    case 'Select':
      return [Select as any, props]
    case 'Select.Option':
      return [Select.Option as any, props]
    case 'Grid':
      return [Grid.Default, props]
    case 'Provider':
      return [Provider.Default, props]
    case 'Modal':
      return [Modal as any, props]
    case 'Radio.Group':
      return [Radio.Group as any, props]
    case 'Radio':
      return [Radio as any, props]
    case 'Slider':
      return [Slider as any, props]
    case 'Switch':
      return [Switch as any, props]
    case 'Space':
      return [Space as any, props]
    case 'Tabs':
      return [Tabs as any, props]
    case 'Tabs.TabPane':
      return [Tabs.TabPane as any, props]
    case 'Breadcrumb':
      return [Breadcrumb as any, props]
    case 'Breadcrumb.Item': {
      const hasOverlay = has(props, 'overlay')
      const Overlay =
        hasOverlay && (Tree.render((props as any)?.overlay) as any)
      return [
        Breadcrumb.Item as any,
        { ...props, overlay: hasOverlay ?? <Overlay /> },
      ]
    }
    case 'Typography':
      return [Typography as any, props]
    case 'Typography.Title':
      return [Typography.Title as any, props]
    case 'Typography.Paragraph':
      return [Typography.Paragraph as any, props]
    case 'Typography.Text':
      return [Typography.Text as any, props]
    default:
      return ['div', props]
  }
}
