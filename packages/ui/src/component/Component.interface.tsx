import { MenuItemProps, MenuProps, SubMenuProps } from 'src/menu/Menu'
import {
  GroupProps,
  InputProps,
  PasswordProps,
  SearchProps,
  TextAreaProps,
} from 'src/input/Input'
import { FormItemProps, FormListProps, FormProps } from 'src/form/Form'
import React, {
  Attributes,
  ForwardRefExoticComponent,
  FunctionComponent,
  PropsWithoutRef,
  RefAttributes,
} from 'react'
import { DropDownProps } from 'src/dropdown/Dropdown'
import { AlertProps } from 'src/alert/Alert'
import { OptionsProps, SelectProps } from 'src/select/Select'
import { SliderProps } from 'src/slider/Slider'
import { CardGridProps, CardMetaProps, CardProps } from 'src/card/Card'
import { TextProps } from 'src/html/Html'

export type RenderPropsChildren<P, T = (props?: P) => React.ReactNode> =
  | T
  | Array<T>

export type RenderProps<P> = {
  children: RenderPropsChildren<P>
  ref?: any
} & P

export type PropItem = any & Function & PropValue

// Only prop value of this type is evaluated
export type PropValue = {
  eval?: boolean // evaluate to function
  renderProps?: boolean // Pass props to child
  value: string
}

// This is purely object shape, not concerning React props like PropTypes
export type Props = {
  ctx?: PropItem // Made available to current function props
  [name: string]: PropItem
}

// For ForwardRef components in ComponentFactory
export type PropTypes<P extends Props> = Attributes & P

export type FactoryComponent<P, T extends HTMLElement = any> =
  | FunctionComponent<P>
  // Return type of React.forwardRef
  | ForwardRefExoticComponent<PropsWithoutRef<P> & RefAttributes<T>>

export type ComponentProps =
  // Card
  | CardProps
  | CardGridProps
  | CardMetaProps

  // Form
  | FormProps
  | FormItemProps
  | FormListProps
  // Menu
  | MenuProps
  | MenuItemProps
  | SubMenuProps
  // Input
  | InputProps
  | TextAreaProps
  | PasswordProps
  | SearchProps
  | GroupProps
  // Dropdown
  | DropDownProps
  // Alert
  | AlertProps
  // Text
  | TextProps
  // Select
  | SelectProps
  | OptionsProps
  // Slider
  | SliderProps
