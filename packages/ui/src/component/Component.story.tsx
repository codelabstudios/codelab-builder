import React from 'react'
import { componentPropsData } from 'src/component/Component.data'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Component',
}

export const basic = () => {
  const Component = Tree.render(componentPropsData)

  return <Component />
}
