import React from 'react'
import { Button, Form as AntForm } from 'antd'
import { FormProps as AntFormProps } from 'antd/lib/form/Form'
import { FormItemProps as AntFormItemProps } from 'antd/lib/form/FormItem'
import { StoreValue } from 'rc-field-form/lib/interface'
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons'

// import { FormListProps as AntFormListProps } from 'antd/lib/form/FormList'

// Copy because not exported from antd
interface FieldData {
  name: number
  key: number
  fieldKey: number
}
interface Operation {
  add: (defaultValue?: StoreValue) => void
  remove: (index: number) => void
  move: (from: number, to: number) => void
}

export type FormListRenderProp = ({
  field,
  operation,
}: {
  field: FieldData
  operation: Operation
}) => React.ReactNode

interface AntFormListProps {
  name: string | number | Array<string | number>
  children: Array<FormListRenderProp>
}

export interface FormProps extends AntFormProps {
  renderProps?: any
}
export interface FormItemProps extends AntFormItemProps {
  renderProps?: { field: FieldData; operation: Operation }
}
export interface FormListProps extends AntFormListProps {}

export namespace Form {
  export const Default: React.FC<FormProps> = ({
    children,
    renderProps,
    ...props
  }) => {
    const { initialValues, ...rest } = props

    return (
      <AntForm initialValues={initialValues} {...rest} {...renderProps}>
        {children}
      </AntForm>
    )
  }

  // export const Item: React.FC<FormItemProps> = ({ children, ...props }) => {
  //   const { label, name, renderProps, ...rest } = props
  //
  //   /**
  //    * Used for layout only
  //    */
  //   if (!name) {
  //     return <AntForm.Item>{children}</AntForm.Item>
  //   }
  //
  //   // Convert name to array
  //   const normalizedName = Array.isArray(name) ? name : [name]
  //
  //   const itemName =
  //     renderProps?.field.name !== undefined
  //       ? [renderProps?.field.name, ...normalizedName]
  //       : normalizedName
  //
  //   return (
  //     <AntForm.Item label={label} name={itemName} {...rest}>
  //       {children}
  //     </AntForm.Item>
  //   )
  // }

  export const List: React.FC<FormListProps> = ({ children, ...props }) => {
    const { name } = props

    return (
      <AntForm.List name={name}>
        {(fields, operation) => {
          const { add, remove } = operation

          return (
            <>
              {fields.map((field: FieldData) => {
                return (
                  <div key={field.key}>
                    {children.map(
                      (child: FormListRenderProp, index: number) => {
                        return (
                          <div key={`${index}-${field.key}`}>
                            {child({ field, operation })}
                          </div>
                        )
                      },
                    )}

                    <AntForm.Item key="Form-list--delete">
                      <Button
                        onClick={() => {
                          remove(field.name)
                        }}
                        style={{ width: '100%' }}
                      >
                        <MinusCircleOutlined /> Delete
                      </Button>
                    </AntForm.Item>
                  </div>
                )
              })}

              <AntForm.Item key="Form-list--add">
                <Button
                  onClick={() => {
                    add()
                  }}
                  type="dashed"
                  style={{ width: '100%' }}
                >
                  <PlusOutlined /> Add
                </Button>
              </AntForm.Item>
            </>
          )
        }}
      </AntForm.List>
    )
  }
}
