import { NodeData } from 'src/tree/node/Node.interface'

export const formData: NodeData = {
  type: 'Form',
  props: {
    name: 'basic',
    initialValues: {},
    onFinish: '',
  },
  children: [
    {
      type: 'Form.Item',
      props: {
        label: 'ID',
        name: 'id',
      },
      children: [
        {
          type: 'Input',
          props: {},
        },
      ],
    },
    {
      type: 'Form.Item',
      props: {
        label: 'Checkbox',
        name: 'checkbox',
        valuePropName: 'checked',
      },
      children: [
        {
          type: 'Checkbox',
        },
      ],
    },
    {
      type: 'Form.Item',
      props: {
        label: 'Select',
        name: 'select',
      },
      children: [
        {
          type: 'Select',
          props: {
            defaultValue: 'a',
            style: {
              width: 120,
            },
          },
          children: [
            {
              type: 'Select.Option',
              props: {
                value: 'a',
              },
              children: [
                {
                  type: 'Text',
                  props: {
                    value: 'A',
                  },
                },
              ],
            },
            {
              type: 'Select.Option',
              props: {
                value: 'b',
              },
              children: [
                {
                  type: 'Text',
                  props: {
                    value: 'B',
                  },
                },
              ],
            },
            {
              type: 'Select.Option',
              props: {
                value: 'c',
              },
              children: [
                {
                  type: 'Text',
                  props: {
                    value: 'C',
                  },
                },
              ],
            },
          ],
        },
      ],
    },
    {
      type: 'Form.Item',
      props: {
        label: 'username',
        name: ['user', 'username'],
      },
      children: [
        {
          type: 'Input',
        },
      ],
    },
    {
      type: 'Form.Item',
      props: {
        label: 'email',
        name: ['user', 'email'],
      },
      children: [
        {
          type: 'Input',
        },
      ],
    },
    {
      type: 'Form.List',
      props: {
        name: 'fields',
        label: 'Fields',
      },
      children: [
        {
          type: 'Form.Item',
          props: {
            name: 'name',
            label: 'Name',
          },
          children: [
            {
              type: 'Input',
            },
          ],
        },
        {
          type: 'Form.Item',
          props: {
            name: 'type',
            label: 'Type',
          },
          children: [
            {
              type: 'Input',
            },
          ],
        },
      ],
    },
    {
      type: 'Form.Item',
      children: [
        {
          type: 'Button',
          props: {
            type: 'primary',
            htmlType: 'submit',
          },
          children: [
            {
              type: 'Text',
              props: {
                value: 'Submit',
              },
            },
          ],
        },
      ],
    },
  ],
}
