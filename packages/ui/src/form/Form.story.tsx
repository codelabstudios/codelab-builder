import React from 'react'
import { formData } from 'src/form/Form.data'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Form',
}

export const list = () => {
  const Form = Tree.render(formData)

  return <Form />
}
