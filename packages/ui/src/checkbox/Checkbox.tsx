import React from 'react'
import { Checkbox as AntCheckbox } from 'antd'
import { CheckboxProps as AntCheckboxProps } from 'antd/lib/checkbox'
import { renderPropsChildren } from 'src/props/Props-renderProps'
import { RenderProps } from 'src/component/Component.interface'

export interface CheckboxProps extends AntCheckboxProps {}

export namespace Checkbox {
  export const Default = ({
    children,
    ...props
  }: RenderProps<CheckboxProps>) => {
    return <AntCheckbox {...props}>{renderPropsChildren(children)}</AntCheckbox>
  }
}
