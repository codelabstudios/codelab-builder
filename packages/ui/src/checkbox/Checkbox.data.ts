import { NodeData } from 'src/tree/node/Node.interface'

export const checkboxData: NodeData = {
  type: 'Checkbox',
  children: [
    {
      type: 'Text',
      props: {
        value: 'Checkbox',
      },
    },
  ],
}
