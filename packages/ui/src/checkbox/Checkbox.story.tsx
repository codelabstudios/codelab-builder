import React from 'react'
import { checkboxData } from 'src/checkbox/Checkbox.data'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Checkbox',
}

export const Default = () => {
  const Checkbox = Tree.render(checkboxData)

  return <Checkbox />
}
