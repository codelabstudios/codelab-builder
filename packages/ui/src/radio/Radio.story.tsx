import React from 'react'
import { radioData } from 'src/radio/Radio.data'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Radio',
}

export const Default = () => {
  const Radio = Tree.render(radioData)

  return <Radio />
}
