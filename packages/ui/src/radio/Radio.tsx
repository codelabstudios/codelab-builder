import React from 'react'
import { Radio as AntRadio } from 'antd'
import { RadioProps as AntRadioProps } from 'antd/lib/radio/interface'

export interface RadioProps extends AntRadioProps {}
export interface RadioGroupProps extends AntRadioProps {}

export namespace Radio {
  export const Default: React.FC<RadioProps> = ({ children, ...props }) => {
    return <AntRadio {...props}>{children}</AntRadio>
  }
  export const Group: React.FC<RadioGroupProps> = ({ children, ...props }) => {
    return <AntRadio.Group {...props}>{children}</AntRadio.Group>
  }
}
