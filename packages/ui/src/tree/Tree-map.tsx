import { curry } from 'ramda'
import { CurryMap, hasChildren } from 'src/tree/node/Node.interface'
import { omit } from 'lodash'

export function map<P1 extends object = {}, P2 extends object = P1>(
  srcChildrenKey = 'children',
  targetChildrenKey = 'children',
) {
  return curry<CurryMap<P1, P2>>(
    (mapFn: (node: P1) => P2, node: P1): P2 => {
      const newNode = mapFn(node)

      if (!hasChildren(node, srcChildrenKey)) {
        return newNode
      }

      return {
        // Replace old children key with new one
        ...omit<P2>(newNode, [srcChildrenKey]),
        [targetChildrenKey]: node[srcChildrenKey]?.map(
          map<P1>(srcChildrenKey, targetChildrenKey)(mapFn as any),
        ),
      } as P2
    },
  )
}
