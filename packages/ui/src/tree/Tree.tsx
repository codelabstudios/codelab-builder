import React, { FunctionComponent, PropsWithChildren } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { Node } from 'src/tree/node/Node'
import {
  Appender,
  BuildSubTree,
  GraphAcc,
  HasChildren,
  Mapper,
  NodeData,
  NodeDTO,
  NodeFinderAcc,
  ReactDataNode,
  TreeAcc,
} from 'src/tree/node/Node.interface'
import { Graph } from 'src/graph/Graph'
import { reduce } from 'lodash'
import { curry } from 'ramda'
import { ComponentProps, Props } from 'src/component/Component.interface'
import { isDataNode, isIDNode } from 'src/tree/node/Node.guards'
import { map } from 'src/tree/Tree-map'
import { elementParameterFactory } from 'src/component/ElementFactory'
import { evalPropsWithContext } from 'src/props/Props-eval'
import { filterRenderProps } from 'src/props/Props-renderProps'

export class Tree<P extends Props = {}> {
  // Returns any type because no props are required
  static render<P extends Props = ComponentProps>(
    data: NodeData<P>,
    factory: typeof elementParameterFactory = elementParameterFactory,
  ): FunctionComponent<any> {
    const root = Tree.tree<P>(data)

    /**
     * (1) ctx is passed to props
     *
     * (2) RenderProps are passed down
     */
    const createElement = (node: Node<P>) => {
      const [type, props] = factory<any>(node)

      const renderProps = evalPropsWithContext(filterRenderProps(props))

      const evaluatedProps = evalPropsWithContext(props)

      /* eslint-disable no-param-reassign */
      node.Element = node.hasChildren()
        ? React.createElement(type, evaluatedProps, node.Children(renderProps))
        : React.createElement(type, evaluatedProps)
    }

    Tree.traversePostorder<P>(root, createElement)

    return ({ children }: PropsWithChildren<Partial<P>>) => {
      return root.Element
    }
  }

  static treeAppender<P extends Props>(
    { prev, subTree, parent }: TreeAcc<P>,
    child: NodeDTO<P>,
  ) {
    const childNode = new Node<P>(child)
    const newPrevNode = childNode

    const prevParent = Tree.findNode(parent?.id, subTree)

    if (!prevParent) {
      throw Error(`Node of id ${parent?.id} not found`)
    }

    prevParent.addChild(childNode)

    return {
      prev: newPrevNode,
      subTree,
    }
  }

  static graphAppender<P extends Props>(
    { graph, subTree, parent }: GraphAcc<P>,
    child: NodeDTO<P>,
  ) {
    const node = new Node<P>(child)

    graph.addVertexFromNode(node)

    const parentNode = Tree.findNode(parent?.id, subTree)

    graph.addEdgeFromNodes(parentNode, node)

    return {
      graph,
      subTree,
      parent,
    }
  }

  /**
   * Linked list of nodes
   */
  static tree<P extends Props = ComponentProps>(data: NodeData<P>): Node<P> {
    const mapper: Mapper<NodeData<P>, NodeDTO<P>> = (node): NodeDTO<P> => {
      let id = uuidv4()
      let props = {}
      let type = ''

      if (isDataNode<P>(node)) {
        props = node.props ?? {}
        type = node.type
      } else if (isIDNode(node)) {
        id = node.id
      }

      return {
        id,
        type,
        props: props as P,
        children: node.children as any,
      }
    }

    const mappedData: NodeDTO<P> = map<NodeData<P>, NodeDTO<P>>()(mapper, data)

    const root = new Node<P>(mappedData)

    return reduce<NodeDTO<P>, TreeAcc<P>>(
      mappedData.children ?? [],
      Tree.traverseSubTree<TreeAcc<P>, NodeDTO<P>>(mappedData)(
        Tree.treeAppender,
      ),
      {
        subTree: root,
        prev: root,
        parent: root,
      },
    ).subTree
  }

  static traversePostorder<P extends Props = {}>(
    node: Node<P>,
    cb: (node: Node<P>) => any,
  ) {
    node.children.forEach((child) => {
      Tree.traversePostorder<P>(child, cb)
    })

    cb(node)
  }

  /* eslint-disable react/display-name,no-param-reassign */
  static traversePreorder<P extends Props = {}>(
    node: Node<P>,
    cb: (node: Node<P>) => any,
  ) {
    if (!node) {
      return
    }

    cb(node)

    node.children.forEach((child) => {
      Tree.traversePreorder(child, cb)
    })
  }

  /**
   * Using Vertex/Edge representation
   */
  static graph<P extends Props = {}>(data: NodeData<P>): Graph {
    // Convert data to Node data structure first, nodeFinder requires Node representation
    const root = Tree.tree(data)

    const graph = new Graph({ vertices: [], edges: [] })

    graph.addVertexFromNode(root)

    return reduce<ReactDataNode<P>, GraphAcc<P>>(
      root?.children ?? [],
      Tree.traverseSubTree<GraphAcc<P>, ReactDataNode<P>>(root)(
        Tree.graphAppender,
      ),
      {
        graph,
        subTree: root,
        parent: root,
      },
    ).graph
  }

  static traverseSubTree = <
    SubTree,
    // SubTree extends HasParent<any>,
    Node extends HasChildren<Node>
  >(
    parent?: Node,
  ): any =>
    curry<BuildSubTree<SubTree, Node>>(
      (
        nodeAppender: Appender<SubTree, Node>,
        subTree: SubTree,
        child: Node,
        index: number,
      ): SubTree => {
        const newSubTree: SubTree = nodeAppender(
          { ...subTree, parent },
          child,
          index,
        )

        if (!Node.hasChildren<Node>(child)) {
          return newSubTree
        }

        // At junction of node, returns when all children appended
        return reduce<Node, SubTree>(
          child.children,
          Tree.traverseSubTree<SubTree, Node>(child)(nodeAppender),
          newSubTree,
        )
      },
    )

  static nodeFinder<P extends Props = {}>(
    { id, found, subTree }: NodeFinderAcc<P>,
    child: Node<P>,
  ) {
    if (!subTree || !id) {
      return null
    }

    let foundNode = found

    if (child.id === id) {
      foundNode = child
    }

    return {
      id,
      found: foundNode,
      subTree,
    }
  }

  static findNode<P extends Props = {}>(
    id: string | undefined,
    subTree: Node<P>,
  ): Node<P> | null {
    if (!id) {
      throw new Error(`id is undefined`)
    }

    if (subTree.id === id) {
      return subTree
    }

    return reduce<Node<P> | null, NodeFinderAcc<P>>(
      subTree?.children || [],
      Tree.traverseSubTree<NodeFinderAcc<P>, Node<P>>()(Tree.nodeFinder),
      {
        subTree,
        found: null,
        id,
      },
    ).found
  }
}
