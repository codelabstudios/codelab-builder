import React from 'react'
import v from 'voca'
import { Node } from 'src/tree/node/Node'
import { Mapper } from 'src/tree/node/Node.interface'
import {
  mapData,
  mapDataCustomChildrenKey,
  mapDataLowerProps,
} from 'src/tree/Tree-map.data'
import {
  reducerData,
  reducerDataCustomChildrenKey,
} from 'src/tree/Tree-reducer.data'
import { reduce } from 'src/tree/Tree-reduce'
import { map } from 'src/tree/Tree-map'

describe('Tree mappers', () => {
  type TreeItem = {
    name: string
  }
  const mapper: Mapper<any, TreeItem> = (node) => ({
    ...node,
    props: {
      name: v.decapitalize(node.props?.name),
    },
  })

  it('it maps each node', () => {
    const mappedTreeData = map()(mapper, mapData)

    expect(mappedTreeData).toStrictEqual(mapDataLowerProps)
  })

  it('it maps each node including children', () => {
    const mappedTreeData = map('children', 'myChildren')(mapper, mapData)

    expect(mappedTreeData).toStrictEqual(mapDataCustomChildrenKey)
  })
})

describe('Tree reducers', () => {
  type PageStats = {
    views: number
  }

  const reducer = (total: number, node: Node<PageStats>) => {
    return total + (node.props?.views ?? 0)
  }

  it('it reduces each node', () => {
    const viewCount = reduce()(reducer, 0, reducerData)

    expect(viewCount).toEqual(13)
  })

  it('it reduces each node using custom children key', () => {
    const viewCount = reduce('myChildren')(
      reducer,
      0,
      reducerDataCustomChildrenKey,
    )

    expect(viewCount).toEqual(13)
  })
})
