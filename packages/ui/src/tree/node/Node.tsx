import React, { ReactElement } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { HasChildren, NodeData } from 'src/tree/node/Node.interface'
import { Props, PropTypes } from 'src/component/Component.interface'
import { isDataNode, isIDNode } from 'src/tree/node/Node.guards'

export class Node<P extends Props = {}> {
  public Element: ReactElement = React.createElement('')

  public id: string = uuidv4()

  public type = ''

  // eslint-disable-next-line react/static-property-placement
  public props: PropTypes<P> = {} as PropTypes<P>

  public parent?: Node<P>

  public children: Array<Node<P>> = []

  /**
   * Can take just ID, but fills out other fields
   */
  constructor(node: NodeData<P>) {
    if (isDataNode<P>(node)) {
      this.props = node.props ?? ({} as PropTypes<P>)
      this.type = node.type
    }
    if (isIDNode(node)) {
      this.id = node.id
    }
  }

  get key(): React.Key {
    return (this.props.key as React.Key) ?? this.id
  }

  public addChild(child: NodeData<P> | Node<P>) {
    let childNode: Node<P>

    if (child instanceof Node) {
      childNode = child
    } else {
      childNode = new Node<P>(child)
    }

    this.children.push(childNode)
    childNode.addParent(this)
  }

  public addParent(parent: Node<P>) {
    this.parent = parent
  }

  static hasChildren<TreeNode extends HasChildren<TreeNode>>(node: TreeNode) {
    return !!node.children?.length
  }

  public hasChildren(): boolean {
    return !!this.children.length
  }

  public Children(renderProps: Props): React.ReactNode {
    if (this.children.length === 1) {
      const { Element, key, props } = this.children[0]
      return React.cloneElement(Element, {
        key: key || '0',
        ...renderProps,
      })
    }

    return this.children.map(({ key, Element }, index) => {
      return React.cloneElement(Element, {
        key: key ?? index,
        ...renderProps,
      })
    })
  }
}
