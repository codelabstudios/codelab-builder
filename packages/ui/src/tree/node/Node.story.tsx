import React from 'react'
import { componentData } from 'src/tree/Tree-component.data'
import { D3Tree, D3TreeData } from 'src/d3/tree/D3Tree'
import { Node } from 'src/tree/node/Node'
import { D3Graph } from 'src/d3/graph/Graph'
import { Mapper } from 'src/tree/node/Node.interface'
import { Tree } from 'src/tree/Tree'
import { map } from 'src/tree/Tree-map'

export default {
  component: Node,
  title: 'Node',
}

export const tree = () => {
  const nodeTree = Tree.tree(componentData)
  console.log(nodeTree)

  const treeMapper: Mapper<any, D3TreeData> = (node) => {
    return {
      id: node.id,
      label: node.id,
    }
  }
  const mappedTree = map<any, D3TreeData>('children', 'children')(
    treeMapper,
    nodeTree,
  )

  console.log(mappedTree)

  return <D3Tree data={mappedTree} />
}

export const graph = () => {
  const nodeGraph = Tree.graph(componentData)
  const { nodes, links } = nodeGraph.D3Graph

  console.log(nodeGraph.D3Graph)

  return <D3Graph nodes={nodes} links={links} />
}
