import { Node } from 'src/tree/node/Node'
import { Graph } from 'src/graph/Graph'
import * as _ from 'ts-toolbelt'
import { Props, PropValue } from 'src/component/Component.interface'

export interface HasChildren<T> {
  children?: Array<T>
}

export interface HasParent<SubTree> {
  parent?: SubTree
}

export type Curry<F extends (...args: any) => any> = _.F.Curry<F>

export interface HasID {
  id: string
}

export interface NodeFinderAcc<P extends Props> extends HasParent<any> {
  subTree: Node<P>
  found: Node<P> | null // found node
  id: string // id we want to search for
}

export type Appender<
  SubTree,
  // SubTree extends HasParent<any>,
  TreeNode extends HasChildren<TreeNode>
> = (acc: SubTree, curr: TreeNode, index: number) => SubTree

export type BuildSubTree<SubTree, TreeNode extends HasChildren<TreeNode>> = (
  nodeAppender: Appender<SubTree, TreeNode>,
  subTree: SubTree,
  child: TreeNode,
  childIndex: number,
) => SubTree

export interface ReactDataNode<P> extends HasChildren<ReactDataNode<P>> {
  type: string
  ctx?: PropValue // pass into props as ctx
  props?: P
}

export interface IDNode<P = any> extends HasChildren<IDNode<P>> {
  id: string
  props?: P
}

export type NodeData<P = any> = ReactDataNode<P> | IDNode<P>

export interface NodeDTO<P = {}> extends HasChildren<NodeDTO<P>> {
  id: string
  type: string
  ctx?: PropValue
  props: P
}

export interface TreeAcc<P extends Props> extends HasParent<NodeDTO<P>> {
  subTree: Node<P>
  prev: Node<P>
}

export interface GraphAcc<P extends Props> extends HasParent<NodeDTO<P>> {
  graph: Graph
  subTree: Node<P>
}

export const hasChildren = <T extends any>(
  node: HasChildren<T>,
  childrenKey = 'children',
) =>
  typeof node === 'object' &&
  typeof node[childrenKey] !== 'undefined' &&
  node[childrenKey]?.length > 0

export type Mapper<T1, T2 = T1> = (node: T1) => T2

export type CurryReduce<T, R> = (reducerFn: Function, init: R, node: T) => R

export type CurryMap<T1, T2> = (mapFn: Mapper<T1, T2>, node: T1) => T2

// export type CurryMap<P1 = {}, P2 = P1> = (
//   mapFn: (node: P1) => DataNode<P2>,
//   node: DataNode<P1>,
// ) => DataNode<P2>
