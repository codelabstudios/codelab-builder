import { NodeData } from 'src/tree/node/Node.interface'

export const treeData: NodeData = {
  id: 'Root',
  children: [
    {
      id: 'A',
      children: [
        {
          id: 'B',
          children: [
            {
              id: 'C',
            },
            {
              id: 'D',
            },
          ],
        },
      ],
    },
    {
      id: 'E',
      children: [
        {
          id: 'F',
        },
        {
          id: 'G',
        },
        {
          id: 'H',
        },
      ],
    },
  ],
}
