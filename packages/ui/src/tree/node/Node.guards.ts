import { IDNode, ReactDataNode } from 'src/tree/node/Node.interface'

export function isDataNode<P>(
  node: ReactDataNode<P> | IDNode<P>,
): node is ReactDataNode<P> {
  return !!(node as ReactDataNode<P>).type
}

export function isIDNode<P>(
  node: ReactDataNode<P> | IDNode<P>,
): node is IDNode<P> {
  return !!(node as IDNode<P>).id
}
