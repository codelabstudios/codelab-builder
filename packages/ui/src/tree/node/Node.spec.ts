import { Node } from 'src/tree/node/Node'
import { treeData } from 'src/tree/node/Tree.data'
import { treeDataWithLabel } from 'src/tree/node/Tree-withLabel.data'
import { Vertex } from 'src/graph/Vertex'
import { Edge } from 'src/graph/Edge'
import { IDNode, Mapper } from 'src/tree/node/Node.interface'
import { D3TreeData } from 'src/d3/tree/D3Tree'
import { Tree } from 'src/tree/Tree'
import { map } from 'src/tree/Tree-map'

/* eslint-disable @typescript-eslint/camelcase */
describe('Node', () => {
  it('can add a child', () => {
    const parent = new Node({ id: 'parent' })
    const child = new Node({ id: 'child' })

    expect(parent.hasChildren()).toBeFalsy()

    parent.addChild(child)

    expect(parent.hasChildren()).toBeTruthy()

    const { children } = parent

    expect(children).toContain(child)
  })

  it('can add a second child', () => {
    const parent = new Node({ id: 'parent' })
    const child = new Node({ id: 'child' })
    const secondChild = new Node({ id: 'secondChild' })

    parent.addChild(child)
    parent.addChild(secondChild)

    const { children } = parent

    expect(children).toEqual([child, secondChild])
  })

  it('can find a node', () => {
    const parent = new Node({ id: 'parent' })
    const child = new Node({ id: 'child' })
    const secondChild = new Node({ id: 'secondChild' })
    const grandChild = new Node({ id: 'grandChild' })

    parent.addChild(child)
    parent.addChild(secondChild)
    secondChild.addChild(grandChild)

    const found = Tree.findNode(grandChild?.id, parent)

    expect(found).toBe(grandChild)
  })

  it('can build a tree from json', () => {
    const tree = Tree.tree(treeData)

    expect(tree.id).toEqual('Root')

    const { children } = tree

    expect(children).toMatchObject([{ id: 'A' }, { id: 'E' }])

    const A = Tree.findNode('A', tree)
    const E = Tree.findNode('E', tree)

    expect(A?.children).toMatchObject([{ id: 'B' }])
    expect(E?.children).toMatchObject([{ id: 'F' }, { id: 'G' }, { id: 'H' }])

    const B = Tree.findNode('B', tree)

    expect(B?.children).toMatchObject([{ id: 'C' }, { id: 'D' }])
  })

  it('has a parent', () => {
    const tree = Tree.tree(treeData)
    const B = Tree.findNode('B', tree)
    const C = Tree.findNode('C', tree)

    expect(C?.parent?.id).toEqual(B?.id)
  })

  it('can map a tree', () => {
    const treeMapper: Mapper<any, D3TreeData> = (node: IDNode) => {
      return {
        id: node.id,
        label: node.id,
      }
    }

    const tree = Tree.tree(treeData)
    const mappedTree = map('children', 'children')(treeMapper, tree)

    expect(mappedTree).toMatchObject(treeDataWithLabel)
  })

  it('can build a graph from json', () => {
    const graph = Tree.graph(treeData)

    const Root = new Vertex({ id: 'Root' })
    const A = new Vertex({ id: 'A' })
    const B = new Vertex({ id: 'B' })
    const C = new Vertex({ id: 'C' })
    const D = new Vertex({ id: 'D' })
    const E = new Vertex({ id: 'E' })
    const F = new Vertex({ id: 'F' })
    const G = new Vertex({ id: 'G' })
    const H = new Vertex({ id: 'H' })

    expect(new Set(graph.vertices)).toMatchObject(
      new Set([Root, A, B, C, D, E, F, G, H]),
    )

    const Root_A = new Edge(Root, A)
    const Root_E = new Edge(Root, E)
    const A_B = new Edge(A, B)
    const B_C = new Edge(B, C)
    const B_D = new Edge(B, D)
    const E_F = new Edge(E, F)
    const E_G = new Edge(E, G)
    const E_H = new Edge(E, H)

    expect(new Set(graph.edges)).toEqual(
      new Set([Root_A, Root_E, A_B, B_C, B_D, E_F, E_G, E_H]),
    )
  })
})
