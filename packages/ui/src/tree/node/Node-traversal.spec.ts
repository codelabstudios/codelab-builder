import { treeData } from 'src/tree/node/Tree.data'
import { Node } from 'src/tree/node/Node'
import { Tree } from 'src/tree/Tree'

describe('Node traversal', () => {
  it('can traverse post order', () => {
    const root = Tree.tree(treeData)
    const queue: Array<string> = []
    const expectedQueue: Array<string> = [
      'C',
      'D',
      'B',
      'A',
      'F',
      'G',
      'H',
      'E',
      'Root',
    ]

    const cb = (node: Node) => {
      queue.push(node.id)
    }

    Tree.traversePostorder(root, cb)

    expect(queue).toEqual(expectedQueue)
  })
})
