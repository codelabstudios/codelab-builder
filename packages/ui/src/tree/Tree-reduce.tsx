import { curry } from 'ramda'
import { Curry, CurryReduce, hasChildren } from 'src/tree/node/Node.interface'

export function reduce<T, R>(
  childrenKey = 'children',
): Curry<CurryReduce<T, R>> {
  return curry<CurryReduce<T, R>>((reducerFn: Function, init: R, node: T) => {
    const acc = reducerFn(init, node)

    if (!hasChildren(node, childrenKey)) {
      return acc
    }

    return node[childrenKey]?.reduce(reduce<T, R>(childrenKey)(reducerFn), acc)
  })
}
