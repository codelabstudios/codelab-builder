import { NodeData } from 'src/tree/node/Node.interface'

export const cardData: NodeData = {
  type: 'Card',
  props: {
    cover:
      '<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />',
    data: {
      renderProps: true,
      value: [1, 2, 3],
    },
  },
  children: [
    {
      type: 'Card.Grid',
      props: {
        hoverable: true,
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'Content',
          },
        },
      ],
    },
    {
      type: 'Card.Grid',
      props: {
        hoverable: true,
      },
      children: [
        {
          type: 'Text',
          props: {
            value: 'Content',
          },
        },
      ],
    },
    {
      type: 'Card.Grid',
      props: {
        hoverable: true,
      },
      children: [
        {
          type: 'Card.Meta',
          props: {
            title: 'Euro Street beat',
            description: 'www.instagram.com',
          },
        },
      ],
    },
  ],
}
