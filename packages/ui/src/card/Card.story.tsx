import React from 'react'
import { cardData } from 'src/card/Card.data'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Card',
}

export const Default = () => {
  const Card = Tree.render(cardData)

  return <Card />
}
