import React from 'react'
import { Card as AntCard } from 'antd'
import { CardProps as AntCardProps } from 'antd/lib/card'
import { CardGridProps as AntCardGridProps } from 'antd/lib/card/Grid'
import { CardMetaProps as AntCardMetaProps } from 'antd/lib/card/Meta'
import { RenderProps } from 'src/component/Component.interface'
import { renderPropsChildren } from 'src/props/Props-renderProps'

export interface CardProps extends AntCardProps {}
export interface CardGridProps extends AntCardGridProps {}
export interface CardMetaProps extends AntCardMetaProps {}

export namespace Card {
  export const Default = ({
    children,
    ...props
  }: RenderProps<Omit<CardProps, 'children'>>) => {
    const { cover, ...rest } = props

    return <AntCard {...rest}>{renderPropsChildren(children, props)}</AntCard>
  }

  export const Grid = ({ children, ...props }: RenderProps<CardGridProps>) => {
    return (
      <AntCard.Grid {...props}>
        {renderPropsChildren(children, props)}
      </AntCard.Grid>
    )
  }

  export const Meta = ({ children, ...props }: RenderProps<CardMetaProps>) => {
    return (
      <AntCard.Meta {...props}>
        {renderPropsChildren(children, props)}
      </AntCard.Meta>
    )
  }
}
