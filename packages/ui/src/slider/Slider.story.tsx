import React from 'react'
import { sliderData } from 'src/slider/Slider.data'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Slider',
}

export const slider = () => {
  const Slider = Tree.render(sliderData)

  return <Slider />
}
