import { SliderProps } from 'src/slider/Slider'
import { NodeData } from 'src/tree/node/Node.interface'

export const sliderData: NodeData<SliderProps> = {
  type: 'Slider',
  props: {
    defaultValue: 20,
    min: 0,
    max: 50,
  },
}
