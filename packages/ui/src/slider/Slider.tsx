import React from 'react'
import { Slider as AntSlider } from 'antd'
import { SliderProps as AntSliderProps } from 'antd/lib/slider'

export interface SliderProps extends AntSliderProps {}

export namespace Slider {
  export const Default: React.FC<SliderProps> = (props) => {
    return <AntSlider {...props} />
  }
}
