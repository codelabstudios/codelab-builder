import React from 'react'
import { Table as AntTable } from 'antd'
import { TableProps as AntTableProps } from 'antd/lib/table'
import { Tree } from 'src/tree/Tree'

// export type DataIndex = { key: number | string }
// export type DataSource<T> = DataIndex & T

export type TableProps<T extends object = any> = AntTableProps<T>

export namespace Table {
  export function Default<T extends object = any>(props: TableProps<T>) {
    const { dataSource, columns } = props

    // Need to convert JSON to component in columns render
    const mappedColumns = columns?.map(({ render, ...column }: any) => {
      if (render) {
        return {
          ...column,
          render: (...cell: any) => {
            const Cell = Tree.render(render)
            return <Cell {...cell} />
          },
        }
      }

      return {
        ...column,
      }
    })

    return <AntTable dataSource={dataSource} columns={mappedColumns} />
  }
}
