import React from 'react'
import { tableData } from 'src/table/Table.data'
import { Tree } from 'src/tree'

export default {
  title: 'Table',
}

export const Default = () => {
  const Table = Tree.render(tableData)

  return <Table />
}
