import { NodeData } from 'src/tree/node/Node.interface'

const deleteButton: NodeData = {
  type: 'Button',
  props: {
    type: 'danger',
    onClick: {
      eval: true,
      value: 'return () => console.log("delete")',
    },
  },
  children: [
    {
      type: 'Text',
      props: {
        value: 'Delete',
      },
    },
  ],
}

const deleteProvider: NodeData = {
  type: 'Provider',
  props: {
    onClick: {
      eval: true,
      value: 'return () => console.log("delete")',
    },
  },
  children: [deleteButton],
}

const editButton: NodeData = {
  type: 'Button',
  props: {
    type: 'warning',
  },
  children: [
    {
      type: 'Text',
      props: {
        value: 'Edit',
      },
    },
  ],
}

export const tableData: NodeData = {
  type: 'Table',
  props: {
    dataSource: [
      {
        key: '1',
        name: 'Mike',
        age: 32,
        address: '10 Downing Street',
      },
      {
        key: '2',
        name: 'John',
        age: 42,
        address: '10 Downing Street',
      },
    ],
    columns: [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Age',
        dataIndex: 'age',
        key: 'age',
      },
      {
        title: 'Address',
        dataIndex: 'address',
        key: 'address',
      },
      {
        title: 'Action',
        dataIndex: 'action',
        key: 'action',
        render: {
          type: 'Space',
          children: [deleteProvider, editButton],
        },
      },
    ],
  },
}
