import React from 'react'
import { buttonData } from 'src/button/Button.data'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Button',
}

export const Default = () => {
  const Button = Tree.render(buttonData)

  return <Button />
}
