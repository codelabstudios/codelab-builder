import { NodeData } from 'src/tree/node/Node.interface'
import { ButtonProps } from 'antd/lib/button'

export const buttonPropKeys: Array<keyof ButtonProps> = [
  'disabled',
  'ghost',
  'href',
  'htmlType',
  'icon',
  'loading',
  'shape',
  'size',
  'target',
  'type',
  'onClick',
  'block',
  'danger',
]

export const buttonData: NodeData<ButtonProps> = {
  type: 'Button',
  props: {
    type: 'primary',
  },
  children: [
    {
      type: 'Text',
      props: {
        value: 'Primary',
      },
    },
  ],
}
