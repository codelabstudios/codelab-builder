import { Vertex } from 'src/graph/Vertex.interface'
import { Edge as IEdge } from 'src/graph/Edge.interface'

export class Edge implements IEdge {
  start: Vertex

  end: Vertex

  id: string

  constructor(start: Vertex, end: Vertex, id?: string) {
    this.start = start
    this.end = end
    this.id = `${start.id}_${end.id}`
  }
}
