import { Vertex } from 'src/graph/Vertex.interface'
import { Edge } from 'src/graph/Edge.interface'

export type GraphProps = {
  vertices: Array<Vertex>
  edges: Array<Edge>
}

export interface Graph {
  vertices: Array<Vertex>
  edges: Array<Edge>
}
