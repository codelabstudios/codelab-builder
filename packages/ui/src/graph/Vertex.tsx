import { HasID } from 'src/tree/node/Node.interface'
import { Vertex as IVertex, VertexProps } from 'src/graph/Vertex.interface'

export class Vertex implements IVertex {
  id: string

  constructor(props: VertexProps) {
    const { id } = props

    this.id = id
  }

  static fromNode(node: HasID): Vertex {
    const { id } = node

    return new Vertex({ id })
  }
}
