import React from 'react'
import { render, screen } from '@testing-library/react'
import { Modal } from 'antd'

describe('Modal', () => {
  // it('renders correctly', () => {
  //   const { asFragment } = render(<Modal title="Modal Title" visible />)
  //
  //   expect(asFragment()).toMatchSnapshot()
  // })

  it('is hidden by default', async () => {
    render(<Modal visible />)
    const modal = await screen.findByRole('dialog')

    expect(modal).toBeVisible()
  })

  // it('shows the modal after clicking the trigger button', async () => {
  //   // const { result: booleanHook } = renderHook(() => useBoolean())
  //   const { truthy, toggle } = useBoolean()
  //
  //   const { getByText, container } = render(
  //     <>
  //       {/*<Button onClick={() => booleanHook.current.toggle()}>Toggle</Button>*/}
  //       {/*<Modal data-test-id="modal" visible={booleanHook.current.truthy} />,*/}
  //       <Button onClick={() => toggle()}>Toggle</Button>
  //       <Modal data-test-id="modal" visible={truthy} />,
  //     </>,
  //   )
  //
  //   fireEvent.click(getByText('Toggle'))
  //
  //   const modal = await screen.findByRole('dialog')
  //
  //   expect(modal).toBeInTheDocument()
  // })
})
