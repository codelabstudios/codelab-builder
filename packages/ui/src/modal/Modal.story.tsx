import React from 'react'
import { Tree } from 'src/tree/Tree'
import { modalData } from 'src/modal/Modal.data'

export default {
  title: 'Modal',
}

export const Default = () => {
  const Modal = Tree.render(modalData)

  return <Modal />
}
