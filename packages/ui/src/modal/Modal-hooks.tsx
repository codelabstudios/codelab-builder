import React, { ReactChildren, useCallback, useContext, useState } from 'react'
import { once } from 'lodash'

export const useBoolean = () => {
  const [truthy, setTruthy] = useState(false)

  const toggle = useCallback(() => setTruthy(!truthy), [truthy])

  return { truthy, toggle }
}

export interface ModalContext<Modal> {
  id: Modal
  visibility: boolean
  children?: ReactChildren
}

// export const ModalContext = React.createContext<Array<ModalContext<Modal>>>([])

// interface UseModal {
//   toggle: Function
//   current: ModalContext
// }

const createModalContext = once(<T,>() =>
  React.createContext([] as Array<ModalContext<T>>),
)
const useModalContext = <T,>() => useContext(createModalContext<T>())

export function useModalProvider<T>(ids: Array<T>) {
  console.log('hello')
  console.log(ids)

  const defaultValues: Array<ModalContext<T>> = ids.map((id) => {
    return {
      id,
      visibility: false,
    }
  })

  console.log(defaultValues)

  function ModalProvider(props: { children: React.ReactChild }) {
    const ModalContext = createModalContext<T>()
    const { children } = props

    return (
      <ModalContext.Provider value={defaultValues}>
        {children}
      </ModalContext.Provider>
    )
  }

  return {
    ModalProvider,
  }
}

export function useModal<T>(id: T) {
  console.log(id)

  const state = useModalContext<T>()

  console.log(state)

  const current = state.find((ctx) => ctx.id === id)

  console.log(current)

  const toggle = () => {
    console.log('toggle')
  }

  return {
    current,
    toggle,
  }
}
