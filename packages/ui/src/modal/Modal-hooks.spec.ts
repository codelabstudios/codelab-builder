import { act, renderHook } from '@testing-library/react-hooks'
import '@testing-library/jest-dom/extend-expect'
import { useBoolean } from 'src/modal/Modal-hooks'

it('toggles visibility', () => {
  const { result } = renderHook(() => useBoolean())

  expect(result.current.truthy).toBeFalsy()

  act(() => result.current.toggle())

  expect(result.current.truthy).toBeTruthy()
})
