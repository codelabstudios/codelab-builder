import { NodeData } from 'src/tree/node/Node.interface'

export const modalData: NodeData = {
  type: 'Provider',
  props: {
    ctx: {
      eval: true,
      renderProps: true,
      value:
        'const [visible, setVisible] = this.React.useState(false); return { visible, setVisible }',
    },
    onOk: {
      eval: true,
      renderProps: true,
      value: 'return () => this.setVisible(false)',
    },
    onCancel: {
      eval: true,
      renderProps: true,
      value: 'return () => this.setVisible(false)',
    },
    visible: {
      eval: true,
      renderProps: true,
      value: 'return this.visible',
    },
    onClick: {
      eval: true,
      renderProps: true,
      value: 'return () => this.setVisible(true)',
    },
  },
  children: [
    {
      type: 'Button',
      children: [
        {
          type: 'Text',
          props: {
            value: 'Toggle Modal',
          },
        },
      ],
    },
    {
      type: 'Modal',
      props: {
        title: 'Basic Modal',
      },
      children: [
        {
          type: 'Html.p',
          children: [
            {
              type: 'Text',
              props: {
                value: 'Some contents...',
              },
            },
          ],
        },
      ],
    },
  ],
}
