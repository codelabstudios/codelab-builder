import React from 'react'
import { typographyData } from 'src/typography/Typography.data'
import { Tree } from 'src/tree/Tree'

export default {
  title: 'Typography',
}

export const Default = () => {
  const Typography = Tree.render(typographyData)
  return <Typography />
}
