import { NodeData } from 'src/tree/node/Node.interface'

export const typographyData: NodeData = {
  type: 'Typography',
  props: {},
  children: [
    {
      type: 'Typography.Title',
      props: {},
      children: [
        {
          type: 'Text',
          props: {
            value: 'Introduction',
          },
        },
      ],
    },
    {
      type: 'Typography.Paragraph',
      props: {},
      children: [
        {
          type: 'Text',
          props: {
            value:
              'In the process of internal desktop applications development, many different design specs and implementations would be involved, which might cause designers and developers difficulties and duplication and reduce the efficiency of development.',
          },
        },
      ],
    },
    {
      type: 'Typography.Paragraph',
      props: {},
      children: [
        {
          type: 'Text',
          props: {
            value:
              'After massive project practice and summaries, Ant Design, a design language for background applications, is refined by Ant UED Team, which aims to',
          },
        },
        {
          type: 'Typography.Text',
          props: {
            strong: true,
          },
          children: [
            {
              type: 'Text',
              props: {
                value:
                  'niform the user interface specs for internal background projects, lower the unnecessary cost of design differences and implementation and liberate the resources of design and front-end development.',
              },
            },
          ],
        },
      ],
    },
  ],
}
