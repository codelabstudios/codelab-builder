import { Injectable } from '@nestjs/common'
import { AxiosRequestConfig } from 'axios'

export type EndpointConfig = Array<{
  name: string
  config: AxiosRequestConfig
}>

@Injectable()
export class Endpoint {
  constructor(private readonly config: EndpointConfig) {}
}
