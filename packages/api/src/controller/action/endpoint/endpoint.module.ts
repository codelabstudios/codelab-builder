import { Module } from '@nestjs/common'
import { endpointProviders } from 'src/controller/action/endpoint/endpoint.providers'

@Module({
  providers: [...endpointProviders],
  exports: [...endpointProviders],
})
export class EndpointModule {}
