import { Injectable } from '@nestjs/common'

export type ActionConfig = Array<{
  name: string
  endpoint: string | Action
}>

@Injectable()
export class Action {
  constructor(private readonly config: ActionConfig) {}

  all() {
    console.log(this.config)
    return this.config
  }
}
