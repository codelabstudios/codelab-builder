import { Controller, Get, Inject } from '@nestjs/common'
import { ACTION_PROVIDER } from 'src/controller/action/action.providers'
import { Action } from 'src/controller/action/action'

@Controller('controller')
export class ControllerController {
  constructor(@Inject(ACTION_PROVIDER) private readonly action: Action) {}

  @Get('')
  index() {
    return this.action.all()
  }
}
