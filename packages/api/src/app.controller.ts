import { Controller, Get } from '@nestjs/common'

@Controller()
export class AppController {
  // constructor(private readonly mongooseService: MongooseService) {}

  @Get('ping')
  ping() {
    return ''
  }

  // @Post('query')
  // async queryMongooseData(@Body() data: QueryData) {}
  //
  // @Post()
  // async saveToMongoose(@Body() data: { models: ReadonlyArray<MongooseModel> }) {
  //   const { models } = data
  //   // const schema = new DynamicSchema(models);
  // }
}
