import { bootstrap } from 'src/server'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const findConfig = require('findup-sync')
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { config } = require('dotenv')

config({ path: findConfig('.env.dev') ?? '' })

bootstrap()
