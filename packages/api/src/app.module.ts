import { Module } from '@nestjs/common'
import { AppController } from 'src/app.controller'
import { ConfigModule, ConfigService } from '@nestjs/config'
import * as Joi from '@hapi/joi'
import { APP_FILTER } from '@nestjs/core'
import { AllExceptionsFilter } from 'src/allException.filter'
import { ModelModule } from './model/model.module'
import { ViewModule } from './view/view.module'
import { ControllerModule } from './controller/controller.module'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const findConfig = require('findup-sync')

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      // load: [configuration],
      envFilePath: findConfig('.env.dev') ?? '',
      validationSchema: Joi.object({
        MONGO_ENDPOINT: Joi.string().required(),
        MONGO_ENDPOINT_DEMO: Joi.string().required(),
      }),
    }),
    ModelModule,
    ViewModule,
    ControllerModule,
  ],
  controllers: [AppController],
  providers: [
    ConfigService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  exports: [ConfigService],
})
export class AppModule {}
