import { Controller, Get, Inject } from '@nestjs/common'
import { DATA_PROVIDER } from 'src/model/data/data.providers'
import { Data } from 'src/model/data/data'

@Controller('model')
export class ModelController {
  constructor(@Inject(DATA_PROVIDER) private readonly data: Data) {}

  @Get('')
  index() {
    return this.data.all()
  }
}
