import { Module } from '@nestjs/common'
import { SchemaModule } from 'src/model/schema/schema.module'
import { Data } from 'src/model/data/data'
import { DataModule } from 'src/model/data/data.module'
import { ModelController } from './model.controller'

@Module({
  imports: [SchemaModule, DataModule],
  exports: [SchemaModule],
  providers: [Data],
  controllers: [ModelController],
})
export class ModelModule {}
