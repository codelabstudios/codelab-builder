import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common'
import { Request } from 'express'
import { iif, Observable, of, throwError } from 'rxjs'
import { catchError, map, switchMap, tap } from 'rxjs/operators'
import { pipe as fPipe } from 'fp-ts/lib/pipeable'
import { PathReporter } from 'io-ts/lib/PathReporter'
import * as t from 'io-ts'
import { fold, left } from 'fp-ts/lib/Either'
import { DecodeError, ModelFromFormData } from '@codelab/common'

type Decode = (obs: Observable<any>) => Observable<any>

export function decode<
  C extends t.Type<A, O, I>,
  A = any,
  O = any,
  I = unknown
>(codec: C) {
  return switchMap((json: I) => {
    return fPipe(
      codec.decode(json),
      fold(
        (error) => {
          return throwError(
            new DecodeError(PathReporter.report(left(error)).join('\n')),
          )
        },
        (data) => {
          return of(data)
        },
      ),
    )
  })
}

@Injectable()
export class SchemaInterceptor<T> implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request: Request = context.switchToHttp().getRequest()

    const preInterceptor = of(request.body).pipe(
      decode(ModelFromFormData),
      tap((data) => {
        request.body = data
      }),
    )

    const postInterceptor = () =>
      next.handle().pipe(
        map((response) => {
          return response.data
        }),
        catchError((error) => {
          return throwError(error.response.data)
        }),
      )

    return iif(() => request.method === 'POST', preInterceptor, of([])).pipe(
      switchMap(postInterceptor),
    )
  }
}
