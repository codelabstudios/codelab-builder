import * as mongoose from 'mongoose'

export interface Field extends mongoose.Document {
  readonly definition: mongoose.Schema.Types.Mixed
}
