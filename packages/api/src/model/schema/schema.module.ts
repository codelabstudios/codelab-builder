import {
  HttpModule,
  MiddlewareConsumer,
  Module,
  NestModule,
} from '@nestjs/common'
import { InjectModel, MongooseModule } from '@nestjs/mongoose'
import restify from 'express-restify-mongoose'
import { ConfigService } from '@nestjs/config'
// import { Model as MongooseModel } from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
import { SchemaController } from 'src/model/schema/schema.controller'
import { FieldSchema } from '@codelab/common'
import { SchemaMiddleware } from 'src/model/schema/schema.middleware'
import { router } from 'src/router'
import { Field } from 'src/model/schema/field/interfaces/field.interface'
import * as mongoose from 'mongoose'

@Module({
  imports: [
    HttpModule.registerAsync({
      useFactory: (config: ConfigService) => ({
        baseURL: config.get('API_ENDPOINT'),
        headers: {
          'Content-Type': 'application/json',
        },
      }),
      inject: [ConfigService],
    }),
    MongooseModule.forRootAsync({
      connectionName: 'codelab-schema',
      useFactory: (configService: ConfigService) => {
        const uri = configService.get<string>('MONGO_ENDPOINT_SCHEMA')
        return {
          autoIndex: true,
          uri,
          connectionFactory: (connection) => {
            // connection.plugin(processSchema)
            connection.plugin(uniqueValidator)
            return connection
          },
        }
      },
      inject: [ConfigService],
    }),
    MongooseModule.forFeature(
      [
        // {
        //   name: 'Model',
        //   schema: ModelSchema.mongoose,
        // },
        {
          name: 'Field',
          schema: FieldSchema,
        },
        // {
        //   name: 'ModelRef',
        //   schema: ModelRefSchema.mongoose,
        // },
        // {
        //   name: 'FieldRef',
        //   schema: FieldSchema.mongoose,
        // },
      ],
      'codelab-schema',
    ),
  ],
  controllers: [SchemaController],
})
export class SchemaModule implements NestModule {
  constructor(
    @InjectModel('Field') private readonly field: mongoose.Model<Field>,
  ) {
    restify.serve(router, field)
  }

  configure(consumer: MiddlewareConsumer) {
    consumer.apply(SchemaMiddleware).forRoutes(SchemaController)
  }
}
