import { Catch, Injectable, NestMiddleware } from '@nestjs/common'
import { Request, Response } from 'express'

// import { router } from 'src/index'

@Catch()
@Injectable()
export class SchemaMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: Function) {
    next()
  }
}
