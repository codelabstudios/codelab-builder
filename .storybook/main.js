const customWebpack = require('./webpack.custom')
const mergeWebpack = require('webpack-merge')
const packages = ['ui', 'common', 'core']

module.exports = {
  stories: packages.map((pkg) => `../packages/${pkg}/src/**/*.story.tsx`),
  addons: ['@storybook/preset-scss'],
  webpackFinal: async (config) => {
    config.module.rules.shift() // remove default babel-loader
    return mergeWebpack([config, customWebpack])
  },
}
