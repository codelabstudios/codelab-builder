import * as React from 'react'
import { addDecorator } from '@storybook/react'
// import results from './.jest-test-results.json'
import './storybook.scss'
import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'
import 'antd/dist/antd.css'

const { Suspense } = React

const withSuspense = (storyFn: any) => {
  return <Suspense fallback={<h1>waiting...</h1>}>{storyFn()}</Suspense>
}

// addDecorator(
//   withTests({
//     filesExt: '(.spec.tsx?)$',
//     results,
//   }),
// )
addDecorator(withSuspense)
