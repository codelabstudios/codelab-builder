const path = require('path')
// const webpack = require('webpack')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
//   .BundleAnalyzerPlugin
const packages = ['ui', 'common', 'core']

module.exports = {
  module: {
    rules: [
      {
        test: /\.(tsx?)$/,
        exclude: /.*node_modules.*/,
        use: [
          {
            loader: require.resolve('babel-loader'),
            options: {
              presets: [
                '@babel/preset-env',
                '@babel/preset-react',
                '@babel/preset-typescript',
              ],
              plugins: [
                [
                  '@babel/plugin-transform-typescript',
                  {
                    allowNamespaces: true,
                  },
                ],
                // [
                //   'import',
                //   {
                //     libraryName: 'antd',
                //     libraryDirectory: 'lib',
                //     style: 'css',
                //   },
                // ],
                '@babel/plugin-proposal-class-properties',
              ],
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.json'],
    modules: packages.map((pkg) => path.resolve(__dirname, '../packages', pkg)),
  },
}
