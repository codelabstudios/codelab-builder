/**
 * Used to generate graphql binding, schema, types etc.
 */
import { config } from 'dotenv'
import {
  build,
  buildPackages
} from './packages/ci-cd/scripts/workflow/gulpfile.build'
import { envPath } from './packages/ci-cd/scripts/workflow/gulpfile.codegen'
import {
  dev,
  devLite,
  devRestart,
  predev,
  preinstall,
  reset,
  seed
} from './packages/ci-cd/scripts/workflow/gulpfile.dev'
import {
  komposeConvert,
  kubeCreateSecret,
  kubeDeploy,
  kubeUpdate
} from './packages/ci-cd/scripts/workflow/gulpfile.kubernetes'
import {
  eslint,
  prettify
} from './packages/ci-cd/scripts/workflow/gulpfile.lint'
import { prod } from './packages/ci-cd/scripts/workflow/gulpfile.prod'
import { terraformUpdate } from './packages/ci-cd/scripts/workflow/gulpfile.terraform'
import { test } from './packages/ci-cd/scripts/workflow/gulpfile.test'

config({ path: envPath })

/**
 * Dev
 */
exports.preinstall = preinstall
exports.dev = dev
exports.predev = predev
exports.devLite = devLite
exports.devRestart = devRestart
exports.seed = seed
exports.reset = reset

/**
 * Lint
 */
exports.eslint = eslint
exports.prettify = prettify

/**
 * Build
 */
exports.buildPackages = buildPackages

/**
 * Prod
 */
exports.build = build
exports.prod = prod

/**
 Test
 */
exports.test = test

/**
 Kubernetes
 */
exports.kubeCreateSecret = kubeCreateSecret
exports.kubeDeploy = kubeDeploy
exports.komposeConvert = komposeConvert
exports.kubeUpdate = kubeUpdate

/**
 * Terraform
 */

exports.terraformUpdate = terraformUpdate
