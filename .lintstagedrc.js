const { CLIEngine } = require('eslint')
const cli = new CLIEngine({})

module.exports = {
  '**/*.{ts,tsx}': (files) => {
    console.log(files)

    const processedFiles = files
      .map((file) => `${file}`)
      .filter((file) => !cli.isPathIgnored(file))
      .join(' ')

    // const cmd = `eslint --max-warnings 0 ${processedFiles}`;
    const cmd = `eslint --fix ${processedFiles}`

    console.log(`Running: ${cmd}`)
    return cmd
  },
  // '*.{ts,tsx,json,graphql,md}': files => `gulp prettify --files
  // ${files.filter(file => true)}`,
}
