const path = require('path')

const packages = ['ui']

const tsConfig = path.resolve(__dirname, 'packages', 'tsconfig.common.json')

const overrideJestConfig = (jestConfig, packageName) => {
  return Object.assign(jestConfig, {
    displayName: `@codelab/${packageName}`,
    rootDir: path.resolve('packages', packageName),
    moduleDirectories: [
      'node_modules',
      path.resolve('packages', packageName, 'node_modules'),
    ],
    modulePaths: ['<rootDir>'],
    watchPathIgnorePatterns: ['<rootDir>/node_modules', '<rootDir>/dist'],
    testPathIgnorePatterns: ['<rootDir>/node_modules', '<rootDir>/dist'],
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'css', 'scss'],
    moduleNameMapper: {
      '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
        '<rootDir>/__mocks__/fileMock.js',
      '\\.(css|scss)$': 'identity-obj-proxy',
    },
    setupFilesAfterEnv: [`${__dirname}/jest.setup.js`],
    testMatch: ['<rootDir>/src/**/?(*.)+(spec).(j|t)s?(x)'],
    transformIgnorePatterns: ['node_modules/(?!(deepdash-es|lodash-es)/)'],
  })
}

module.exports = {
  rootDir: __dirname,
  testMatch: ['<rootDir>/src/**/?(*.)+(spec).(j|t)s?(x)'],
  projects: packages.map((pkg) =>
    // eslint-disable-next-line global-require,import/no-dynamic-require
    overrideJestConfig(require(`./packages/${pkg}/jest.config`), pkg),
  ),
  globals: {
    'ts-jest': {
      tsConfig,
      // babelConfig: require(path.resolve(__dirname, 'babel.config.js')),
    },
  },
}
