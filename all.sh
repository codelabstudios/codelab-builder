#!/usr/bin/env bash

# Usage
#
# ./all.sh ncu -u package_a package_b
#
rootpath=$PWD
for i in packages/*;
do
  cd "${rootpath}/${i}"
  "$@"
done

