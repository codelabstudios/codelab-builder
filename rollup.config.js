import nodeResolve from '@rollup/plugin-node-resolve'
import path from 'path'
import autoExternal from 'rollup-plugin-auto-external'
// import babel from 'rollup-plugin-babel'
import commonjs from 'rollup-plugin-commonjs'
import builtins from 'rollup-plugin-node-builtins'
import globals from 'rollup-plugin-node-globals'
import tsPlugin from 'rollup-plugin-typescript2'
import ttypescript from 'ttypescript'

// import React from 'react'

// const packages = ['ui', 'common', 'utils']
const packages = ['common', 'ui', 'eslint-config-codelab', 'utils']

export default packages.map((pkg) => {
  return {
    input: path.resolve(__dirname, `packages/${pkg}/src/index.ts`),
    output: [
      {
        dir: path.resolve(__dirname, `packages/${pkg}/dist`),
        name: pkg,
        format: 'es',
        // format: 'iife',
        sourcemap: true,
        extend: true,
      },
    ],
    external: [
      'lodash',
      'antd',
      'react',
      'react-dom',
      'mongoose',
      'io-ts',
      'voca',
      'axios',
      'react-draggable',
    ],
    plugins: [
      // Needed to import utils
      globals(),
      builtins(),
      nodeResolve({
        // pass custom options to the resolve plugin
        customResolveOptions: {
          moduleDirectory: 'node_modules',
        },
        // browser: true,
        // resolveOnly: ['deepdash-es', 'lodash-es'],
        // modulesOnly: true,
        // extensions: ['.js', '.ts'],
        dedupe: ['react', 'react-dom'], // Default: []
      }),
      autoExternal({
        builtins: false,
        dependencies: true,
        packagePath: path.resolve(__dirname, `packages/${pkg}/package.json`),
        peerDependencies: false,
      }),
      commonjs({
        include: '**/node_modules/**',
        // namedExports: {
        //   'react-is': Object.keys(reactIs),
        //   // 'node_modules/react-is/index.js': ['isFragment', 'ForwardRef'],
        // },
      }),
      // babel(),

      tsPlugin({
        typescript: ttypescript,
        tsconfig: path.resolve(__dirname, `packages/${pkg}/tsconfig.json`),
      }),
    ],
  }
})
